/*
 Navicat Premium Data Transfer

 Source Server         : _myconn
 Source Server Type    : MySQL
 Source Server Version : 50532
 Source Host           : localhost:3306
 Source Schema         : mui_tangerangdb

 Target Server Type    : MySQL
 Target Server Version : 50532
 File Encoding         : 65001

 Date: 25/04/2020 08:41:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for agenda
-- ----------------------------
DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda`  (
  `id_agenda` int(5) NOT NULL AUTO_INCREMENT,
  `tema` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tema_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `isi_agenda` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tempat` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `pengirim` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `tgl_posting` date NOT NULL,
  `jam` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT 1,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_agenda`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 65 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agenda
-- ----------------------------
INSERT INTO `agenda` VALUES (64, 'Lomba menggambar TK Al-Hikmah', 'lomba-menggambar-tk-alhikmah', '', 'Pizza Hut Tangerang', '', '927459Lomba TK-PAUD Al-Hikmah di Pizza Citra Raya.jpeg', '2019-11-17', '2019-11-17', '2012-08-20', '09.00 - 21.00 Wib', 26, 'admin');
INSERT INTO `agenda` VALUES (62, 'Sparing Antara Tim Futsal TK', 'sparing-antara-tim-futsal-tk', '', 'Istora Senayan Jakarta', '', '36132sparing.jpeg', '2018-10-05', '2018-10-05', '2012-08-19', '09.00 - 21.00 Wib', 23, 'admin');
INSERT INTO `agenda` VALUES (63, 'Peringatan Tahun baru Islam ke 1441 H ', 'peringatan-tahun-baru-islam-ke-1441-h-', 'Bambu Nusantara ke-6 tahun ini akan digelar di Jakarta Convention Center (JCC), di Jalan Jenderal Gatot Subroto, Jakarta, pada 1 - 2 September 2012. Acara tersebut akan menghadirkan beraneka kreasi berbahan bambu dan tampilnya beragam seni dari bambu. Selain suguhan musik etnik berpadu dengan musik modern, dalam Acara ini juga akan turut diisi dengan pameran, seminar, merchandise, kuliner, dan fashion yang dipadu padankan dengan karya berbahan bambu.<br />\r\n', 'Jakarta Convention Center (JCC), Jakarta', '', '700286tHBI.jpeg', '2018-09-01', '2018-09-02', '2012-08-20', '09.00 - 21.00 Wib', 25, 'admin');

-- ----------------------------
-- Table structure for album
-- ----------------------------
DROP TABLE IF EXISTS `album`;
CREATE TABLE `album`  (
  `id_album` int(5) NOT NULL AUTO_INCREMENT,
  `jdl_album` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `album_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gbr_album` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `hits_album` int(5) NOT NULL DEFAULT 1,
  `tgl_posting` date NOT NULL,
  `jam` time NOT NULL,
  `hari` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_album`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of album
-- ----------------------------
INSERT INTO `album` VALUES (30, 'TK', 'tk', 'Para macan tua yang digawangi Iwan Fals, Setiawan Djody dan Sawung Jabo di Stadion Gelora Bung Karno, Jakarta,\r\nJumat (30/12) malam. Kantata kembali membawakan lagu-lagu legendarisnya\r\nsetelah 21 tahun vakum dari dunia musik.\r\n<div style=\"overflow: hidden; color: #000000; background-color: #ffffff; text-align: left; text-decoration: none; border: medium none\">\r\n<br />\r\n</div>\r\n', '40IMG_1384.JPG', 'Y', 36, '2012-08-20', '09:12:06', 'Senin', 'admin');
INSERT INTO `album` VALUES (31, 'PAUD', 'paud', '', '48PAUD Angkatan 2013-2014.JPG', 'Y', 37, '2012-08-20', '09:40:01', 'Senin', 'admin');
INSERT INTO `album` VALUES (28, 'KAJIAN', 'kajian', 'Pasar Asemka, Jakarta, merupakan pasar grosir yang banyak menyediakan berbagai aksesoris seperti kalung, cincin, Souvenir pernakahan, dan lainnya. Di Pasara Asemka anda akan dimanjakan dengan beragam barang yang dibandrol dengan harga murah meriah dan biasanya dijual grosiran. \r\n', '95masjid-alumni-ipb-bogor-setiap-hari-menggelar-kajian-islam-_170719172302-591.jpg', 'Y', 170, '2012-08-18', '23:14:05', 'Sabtu', 'admin');
INSERT INTO `album` VALUES (29, 'TPA', 'tpa', 'Belgia sedang memperingati peristiwa tahunan &quot;La Fete De La Fleur&quot; atau pesta bunga di bulan Agustus. Ahli bunga merancang karpet raksasa dari bunga di depan Grand Place di Brussel. Karpet ini dibuat menggunakan 700 ribu bunga.\r\n', '44tpa.jpg', 'Y', 25, '2012-08-19', '03:02:27', 'Minggu', 'admin');
INSERT INTO `album` VALUES (32, 'SANTUNAN', 'santunan', '', '45Snapshot_4.png', 'Y', 1, '2020-04-24', '15:27:41', 'Jumat', 'admin');

-- ----------------------------
-- Table structure for background
-- ----------------------------
DROP TABLE IF EXISTS `background`;
CREATE TABLE `background`  (
  `id_background` int(5) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_background`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of background
-- ----------------------------
INSERT INTO `background` VALUES (1, 'main-body-bg.jpg');

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner`  (
  `id_banner` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  PRIMARY KEY (`id_banner`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES (17, 'Komunitas 80an', 'http://komunitas80an.blogspot.com', '80an.jpg', '2011-06-26');
INSERT INTO `banner` VALUES (14, 'Beta UFO Indonesia', '', 'betaufo.jpg', '2011-06-22');
INSERT INTO `banner` VALUES (18, 'Lokomedia', 'http://bukulokomedia.com', 'lokomedia2.jpg', '2011-06-26');

-- ----------------------------
-- Table structure for berita
-- ----------------------------
DROP TABLE IF EXISTS `berita`;
CREATE TABLE `berita`  (
  `id_berita` int(5) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(5) NOT NULL,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `sub_judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `youtube` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `judul_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `headline` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `utama` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `isi_berita` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `keterangan_gambar` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `hari` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT 1,
  `tag` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_berita`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 669 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of berita
-- ----------------------------
INSERT INTO `berita` VALUES (624, 53, 'admin', 'Sambutan Ketua Dewan Pakar Al-Hikmah Citra Raya ', '', '', 'sambutan-ketua-dewan-pakar-alhikmah-citra-raya-', 'N', 'N', 'N', '<div align=\"justify\">\r\n<font size=\"2\">Yayasan Al-Hikmah Citra Raya adalah suatu yayasan Islam yang dibentuk dan dikelola dari, oleh dan untuk masyarakat yang memfokuskan diri pada bidang Pendidikan, Da&rsquo;wah, dan Sosial.</font><br />\r\n<font size=\"2\">\r\nDewan pakar akan berfungsi untuk memperkuat kinerja Yayasan Al-Hikmah Citra Raya, terutama hal-hal strategis baik ke dalam maupun keluar. Bisa juga membantu untuk konsolidasi pengurus yayasan. Kami akan selalu memberikan pemikiran sesuai kepakaran kami untuk menganalisis dan memberikan terobosan baru dalam bidang Pendidikan, Da&rsquo;wah dan Sosial, namun tingkat eksekusinya tetap ada di pengurus yayasan.</font><br />\r\n<font size=\"2\">\r\nDewan pakar akan berusaha memberikan inovasi baru agar Yayasan Al-Hikmah Citra Raya dapat tampil lebih elegan dan mampu menghadirkan inovasi-inovasi baru dalam bidang Pendidikan, Da&rsquo;wah dan Sosial ditengah masyarakat digital saat ini, dan semua inovasi tersebut akan sejalan dengan berbagai program pemerintah NKRI. Akhirnya besar harapan kami Yayasan Al-Hikmah Citra Raya akan menjadi mitra pemerintah dalam mensukseskan berbagai programnya.</font><br />\r\n</div>\r\n<font size=\"2\">\r\n<br />\r\n&nbsp;<br />\r\n<br />\r\n</font>\r\n<div align=\"center\">\r\n<font size=\"2\">\r\nWassalamu&rsquo;alaikum Warahmatullahi Wabaraktuh<br />\r\n</font>\r\n<font size=\"2\"><br />\r\n&nbsp;Hormat kami,<br />\r\n</font>\r\n<font size=\"2\"><br />\r\n&nbsp;<br />\r\n</font>\r\n<font size=\"2\"><br />\r\nAssociate. Prof. Dr. Al-Bahra, S.Kom, M.Kom<br />\r\n</font>\r\n<font size=\"2\"><br />\r\nKetua Dewan Pakar Yayasan Al-Hikmah Citra Raya<br />\r\n</font>\r\n</div>\r\n<font size=\"2\">\r\n</font>\r\n', '', 'Senin', '2020-04-20', '10:14:24', '76bahra.jpg', 8, 'metropolitan');
INSERT INTO `berita` VALUES (614, 53, 'admin', 'Sambutan Ketua Yayasan Al-Hikmah Citra Raya', '', '', 'sambutan-ketua-yayasan-alhikmah-citra-raya', 'N', 'N', 'Y', '<font face=\"arial,helvetica,sans-serif\" size=\"2\"><br />\r\n</font>\r\n<div align=\"justify\">\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">\r\nYayasan Al-Hikmah Citra Raya adalah suatu yayasan Islam yang dibentuk dan dikelola dari, oleh dan untuk masyarakat yang memfokuskan diri pada bidang Pendidikan, Da&rsquo;wah, dan Sosial. Sampaikanlah walau satu ayat ! adalah moto dari yayasan Al-Hikmah Citra Raya. Moto yang sederhana tapi cukup untuk menjadi landasan gerak dan semangat dalam setiap aktivitas yang dilaksanakan oleh yayasan ini. Ada dua hal penting yang menjadi dasar dari moto dan semangat tersebut, yaitu:</font><br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">\r\n1.&nbsp;&nbsp; Mensyiarkan dienul Islam kepada seluruh umat manusia dengan berupaya menjadi uswah dalam mewujudkan masyarakat yang beriman dan bertaqwa, seperti firman Allah dalam Al-Qur&rsquo;an surat Al-A&rsquo;raf ayat 96 yang artinya :&rdquo;Jikalau sekiranya penduduk negeri-negeri beriman dan bertakwa, pastilah Kami akan melimpahkan kepada mereka berkah dari langit dan bumi,&nbsp; tetapi mereka mendustakan&nbsp; (ayat-ayat Kami) itu, Maka Kami siksa mereka disebabkan perbuatannya&rdquo;.</font><br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">\r\n2.&nbsp;&nbsp; Terbentuknya Masyarakat Yang Memahami Tujuan Hidup, Pedoman Hidup dan Ikhlas dalam beramal, Seperti Firman Allah Dalam Al-Qur&rsquo;an Surat Adz-Dzariyaat ayat 56 yang artinya, &rdquo;Dan aku tidak menciptakan jin dan manusia melainkan supaya mereka mengabdi kepada-Ku&rdquo;.</font><br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">\r\nDalam rangka mengamalkan moto tersebutlah, kami berusaha menjalankan roda yayasan dengan profesional. Semoga pengunjung sekalian mendapatkan beberapa hal yang diinginkan dari situs ini.</font><br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">\r\n&nbsp;</font><br />\r\n</div>\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">\r\n<br />\r\n</font>\r\n<div align=\"center\">\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">Wassalamu&rsquo;alaikum Warahmatullahi Wabaraktuh</font><br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">&nbsp;</font><br />\r\n<br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">Hormat Kami</font><br />\r\n<br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">&nbsp; </font><br />\r\n<br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">Ust Abdul Qohar, S.Pd</font><br />\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\">Ketua Yayasan Al-Hikmah Citra Raya</font><br />\r\n</div>\r\n<font face=\"arial,helvetica,sans-serif\" size=\"2\"><br />\r\n</font>\r\n', '', 'Jumat', '2020-04-20', '04:59:50', '29abd_qahar.png', 25, 'teknologi');
INSERT INTO `berita` VALUES (660, 36, 'admin', 'Sistem Penilaian TK Al-Hikmah', '', '', 'sistem-penilaian-tk-alhikmah', 'N', 'N', 'N', '<p>\r\nUndang-undang Sistem Pendidikan Nasional No. 20&nbsp; tahun 2003 menyatakan bahwa pendidikan nasional bertujuan untuk mengembangkan potensi peserta didik agar menjadi manusia yang beriman dan bertakwa kepada Tuhan Yang Maha Esa,<br />\r\nberakhlak mulia, sehat, berilmu, cakap, kreatif, mandiri dan menjadi warga negara yang demokratis serta bertanggung jawab. TK sebagai salah satu bentuk lembaga pendidikan usia dini, berada pada jalur pendidikan non-formal. Implikasinya adalah<br />\r\nbahwa keberadaan dan penyelenggaraan TK perlu diatur dalam suatu kebijakan tertentu oleh pemerintah dalam hal ini Departemen Pendidikan Nasional. \r\n</p>\r\n<p>\r\n<br />\r\nSeiring dengan inovasi pendidikan sebagai salah satu realisasi otonomi pendidikan, pemerintah sejak beberapa tahun terakhir telah mengembangkan urikulum 2004 yang berbasis kompetensi. Kurikulum sebagai seperangkat rencana dan pengaturan mengenai tujuan, isi dan bahan pelajaran serta cara yang digunakan sebagai pedoman penyelenggaraan kegiatan pembelajaran untuk mencapai tujuan pendidikan tertentu, disesuaikan dengan keadaan dan kemampuan daerah. kurikulum TK dilaksanakan dalam rangka membantu anak didik mengembangkan berbagai potensi baik fisik maupun psikis yang meliputi moral dan nilai-nilai agama, sosial-emosional dan kemandirian, berbahasa, kognitif, fisik-motorik, dan seni agar siap memasuki pendidikan dasar.\r\n</p>\r\n', '', 'Senin', '2020-04-20', '08:29:20', '32logo.jpg', 0, '');
INSERT INTO `berita` VALUES (603, 42, 'admin', 'PAUD Angkatan 2014-2015', '', '', 'paud-angkatan-20142015', 'Y', 'Y', 'N', 'Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....\r\n', '', 'Sabtu', '2012-11-17', '03:27:34', '35PAUD Angkatan 2014-2015.JPG', 11, 'teknologi');
INSERT INTO `berita` VALUES (604, 39, 'admin', 'Santunan Anak Yatim dan Dhuafa', '', '', 'santunan-anak-yatim-dan-dhuafa', 'Y', 'N', 'N', 'Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....<br />\r\nIsi keterangan disini....Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....Isi keterangan disini....<br />\r\n<br />\r\n<br />\r\n', '', 'Minggu', '2012-08-19', '03:37:05', '9Snapshot_5.png', 14, 'internasional');
INSERT INTO `berita` VALUES (659, 36, 'admin', 'Pedoman Silabus TK Al-Hikmah', '', '', 'pedoman-silabus-tk-alhikmah', 'N', 'N', 'N', 'Prinsip otonomi daerah sesuai dengan Undang-undang No. 32 tahun 2004 tentang Pemerintahan Daerah menuntut pelaksanaan otonomi yang nyata dan bertanggungjawab dalam penyelenggaraan pendidikan sesuai dengan peraturan yang berlaku. Daerah berwenang untuk menangani urusan pendidikan yang dilaksanakan berdasarkan tugas, wewenang, dan kewajiban yang senyatanya telah ada dan berpotensi untuk tumbuh, hidup dan berkembang sesuai dengan potensi dan kekhasan daerah. Selain itu daerah juga harus bertanggungjawab dalam penyelenggaraannya yang benar-benar sejalan dengan tujuan dan maksud pemberian otonomi, yang pada dasarnya untuk memberdayakan daerah termasuk meningkakatkan pelayanan dasar pendidikan yang merupakan bagian utama dari tujuan nasional.<br />\r\n<br />\r\nOtonomi dalam bidang pendidikan yang diwujudkan dalam PP No. 25 tahun 2000 tentang Kewenangan Pemerintah dan Daerah Propinsi sebagai Daerah Otonom, pasal 2 ayat (2) dan (3) dalam bidang pendidikan telah dinyatakan bahwa pemerintah (Pusat) memiliki kewenangan antara lain (1) penetapan standar kompetensi siswa dan warga belajar serta pengaturan kurikulum nasional dan penilaian hasil belajar secara nasional serta pedoman pelaksanaannya, (2)penetapan standar materi pelajaran pokok, (3) penetapan pedoman pembiayaan penyelenggaraan pendidikan, dan (4) penetapan kalender pendidikan dan jumlah<br />\r\njam belajar efektif setiap tahun bagi pendidikan dasar, menengah dan luar sekolah. <br />\r\n', '', 'Senin', '2020-04-20', '08:25:40', '76logo.jpg', 0, '');
INSERT INTO `berita` VALUES (656, 57, 'admin', 'TK Al-Hikmah', '', '', 'tk-alhikmah', 'Y', 'N', 'N', 'Isi keterangan disini....Isi keterangan disini....Isi keterangan \r\ndisini....Isi keterangan disini....Isi keterangan disini....Isi \r\nketerangan disini....Isi keterangan disini....Isi keterangan \r\ndisini....Isi keterangan disini....Isi keterangan disini....\r\n', '', 'Senin', '2020-04-20', '08:13:58', '55IMG_1384.JPG', 0, '');
INSERT INTO `berita` VALUES (657, 36, 'admin', 'Pedoman Silabus PADU Al-Hikmah', '', '', 'pedoman-silabus-padu-alhikmah', 'N', 'N', 'N', 'Prinsip otonomi daerah sesuai dengan Undang-undang No. 32 tahun 2004 tentang Pemerintahan Daerah menuntut pelaksanaan otonomi yang nyata dan bertanggungjawab dalam penyelenggaraan pendidikan sesuai dengan peraturan yang berlaku. Daerah berwenang untuk menangani urusan pendidikan yang dilaksanakan berdasarkan tugas, wewenang, dan kewajiban yang&nbsp; senyatanya telah ada dan berpotensi untuk tumbuh, hidup dan berkembang sesuai dengan potensi dan kekhasan daerah. Selain itu daerah juga harus bertanggungjawab dalam penyelenggaraannya yang benar-benar sejalan dengan tujuan dan<br />\r\nmaksud pemberian otonomi, yang pada dasarnya untuk memberdayakan daerah termasuk meningkaPAUDan pelayanan dasar pendidikan yang merupakan bagian utama dari tujuan nasional.<br />\r\n<br />\r\nOtonomi dalam bidang pendidikan yang diwujudkan dalam PP No. 25 tahun 2000 tentang Kewenangan Pemerintah dan Daerah Propinsi sebagai Daerah Otonom, pasal 2 ayat (2) dan (3) dalam bidang pendidikan telah dinyatakan bahwa pemerintah (Pusat) memiliki kewenangan antara lain (1) penetapan standar kompetensi siswa dan warga belajar serta pengaturan kurikulum nasional dan penilaian hasil belajar secara nasional serta pedoman pelaksanaannya, (2) penetapan standar materi pelajaran pokok, (3) penetapan pedoman pembiayaan penyelenggaraan pendidikan, dan (4) penetapan kalender pendidikan dan jumlah<br />\r\njam belajar efektif setiap tahun bagi pendidikan dasar, menengah dan luar sekolah\r\n', '', 'Senin', '2020-04-20', '08:21:14', '37logo.jpg', 0, '');
INSERT INTO `berita` VALUES (658, 36, 'admin', 'Sistem Penilaian pada PAUD Al-Hikmah ', '', '', 'sistem-penilaian-pada-paud-alhikmah-', 'N', 'N', 'N', '<p>\r\nUndang-undang Sistem Pendidikan Nasional No. 20&nbsp; tahun 2003 menyatakan bahwa pendidikan nasional bertujuan untuk mengembangkan potensi peserta didik agar menjadi manusia yang beriman dan bertakwa kepada Tuhan Yang Maha Esa,<br />\r\nberakhlak mulia, sehat, berilmu, cakap, kreatif, mandiri dan menjadi warga negara yang demokratis serta bertanggung jawab. PAUD sebagai salah satu bentuk lembaga pendidikan usia dini, berada pada jalur pendidikan non-formal. Implikasinya<br />\r\nadalah bahwa keberadaan dan penyelenggaraan PAUD perlu diatur dalam suatu kebijakan tertentu oleh pemerintah dalam hal ini Departemen Pendidikan Nasional. \r\n</p>\r\n<p>\r\nSeiring dengan inovasi pendidikan sebagai salah satu realisasi otonomi pendidikan, pemerintah sejak beberapa tahun terakhir telah mengembangkan Kurikulum 2004 yang berbasis kompetensi. Kurikulum sebagai seperangkat rencana dan pengaturan mengenai tujuan, isi dan bahan pelajaran serta cara yang digunakan sebagai pedoman penyelenggaraan kegiatan pembelajaran untuk mencapai tujuan pendidikan tertentu, disesuaikan dengan keadaan dan kemampuan daerah. Kurikulum PAUD dilaksanakan dalam rangka membantu anak didik mengembangkan berbagai potensi baik fisik maupun psikis yang meliputi moral dan nilai-nilai agama, sosial-emosional dan kemandirian, berbahasa, kognitif, fisik-motorik, dan seni agar siap memasuki pendidikan dasar.\r\n</p>\r\n', '', 'Senin', '2020-04-20', '08:22:56', '89logo.jpg', 0, '');
INSERT INTO `berita` VALUES (668, 55, 'admin', 'Biografi Ketua Yayasan', '', '', 'biografi-ketua-yayasan', 'N', 'N', 'N', '<div>\r\nAbdul Qohar lahir di Jakarta, 28 Januari 1996. Sarjana Pendidikan dari Universitas Islam Nusantara. Diploma Bahasa Arab dari Maâ€™had Al-Imarot Kota Bandung. Saat ini sedang menempuh program peendidikan Magister Manajemen. Pengajar Bahasa Arab pada Pondok Pesantren Daarul Muqorrabon.Â \r\n</div>\r\n<div>\r\nPernah mondok di Pondok Pesantren Al-Amanah Boarding School dan Pondok Pesantren Darul Muqorrobin. Sudah menjadi tim penulis buku Pendidikan Agama Islam untuk Perguruan Tinggi. Aktif mengisi kajian-kajian keislaman seputar Aqidah, Akhlaq dan Fiqih, serta memberikan khutbah jumâ€™at dan khutbah idul fitri\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n', '', 'Sabtu', '2020-04-25', '06:04:23', '46abdul_qohar.jpg', 0, '');
INSERT INTO `berita` VALUES (667, 55, 'admin', 'Biografi Ketua Dewan Pakar', '', '', 'biografi-ketua-dewan-pakar', 'N', 'N', 'N', '<div>\r\nAl-Bahra bin Ladjamuddin lahir di Jakarta, 2 Maret 1971. Doktor Teknologi Pendidikan dari Universitas Negeri Jakarta. Jabatan Akademik Dosen saat ini adalah Associate Profesor. Dosen Tetap pada Program Studi Pascasarjana Magister Teknik Informatika Universitas Raharja, Tangerang - Banten&nbsp;\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\nMulai mengajar di perguruan tinggi sejak tahun 1994 (sudah 26 tahun) pada FE Universitas Islam Assyafiiyah Jakarta. Sudah mengajar lebih dari 20 perguruan tinggi di daerah Bandung, Karawang, Jakarta, Tangerang, Serang dan Lampung. Sejak tahun 1994 sudah aktif dalam kegiatan pendidikan dan pengajaran, membimbing skripsi tesis dan disertasi, Sudah menulis 20 (dua puluh) buku ajar ilmu komputer untuk perguruan tinggi, melakukan penelitian bidang ilmu komputer, pendidikan, kebidanan, keperawatan dan kesehatan masyarakat, dengan luaran berupa jurnal ilmiah nasional dan internasional diantaranya adalah:&nbsp;&nbsp;\r\n</div>\r\n<div>\r\n1.<span style=\"white-space: pre\">	</span>Influences Of Health Promotion And Motivation Towards Clean And Healthy Behavior (PHBS) On Household Orders In Depok, page 162-171\r\n</div>\r\n<div>\r\nElla Nurlelawati, Novi Ernawati, Maimunah, Albahra, Indonesia, Volume 3, Issue 4, Aug 2018. http://www.ijaemr.com/view/4/2018\r\n</div>\r\n<div>\r\n2.<span style=\"white-space: pre\">	</span>Factors Affecting Mother&#39;s Knowledge Level About Flour Albus At Puskesmas L In 2017, page 116-127\r\n</div>\r\n<div>\r\nDewi Sartika Sembiring, Masayu Delta, Albahra, Indonesia, Volume 3, Issue 5 , Oct 2018, http://www.ijaemr.com/view/5/2018\r\n</div>\r\n<div>\r\n3.<span style=\"white-space: pre\">	</span>The Relationship of Learning Methods, Knowledge, Attitudes, Interests, and Motivation with Self-Study Behavior of Fourth Semester Students in INC Laboratory Practices at the Midwifery Academy Bhakti Asih Ciledug, Hj. Sumarmi, Al-Bahra, http://icosheet.umkudus.ac.id/paper-submission/\r\n</div>\r\n<div>\r\n4.<span style=\"white-space: pre\">	</span>Hubungan Antara Manajemen Waktu Dengan Prestasi Belajar Mahasiswa Akademi Keperawatan RSPAD Gatot Soebroto Jakarta, Al- Bahra, Memed Sena Setiawan,&nbsp;\r\n</div>\r\n<div>\r\nhttps://ejournal.akperrspadjakarta.ac.id/index.php/JEN/issue/view/10\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\nSudah menulis 5 (lima) buku agama Islam untuk masyarakat dan perguruan tinggi, Sudah mempublikasikan 400 lebih tulisan tentang aqidah, syari&rsquo;ah, akhlaq, ibadah, mu&rsquo;amalah, dan sirroh pada www.ustalbahra-nurulhidayah.or.id, aktif memberikan ceramah, khutbah jum&rsquo;at, khutbah idul fitri, khutbah idul adha\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\nSebagai staff ahli akreditasi program studi dan akreditasi institusi perguruan tinggi. Sampai saat ini sudah dipercaya sebagai Chief Editor, Editor Eksekutif, Ketua Dewan Redaksi dan Menjadi Mitra Bestari, Serta Penyunting Ahli 33 (tiga puluh tiga) Jurnal Ilmiah Nasional bidang ilmu Komputer, Pendidikan, Kebidanan, Keperawatan, Kesehatan Masyarakat, Farmasi, Kesehatan Lingkungan, Teknik dan Manajemen Industri, Teknik Elektro dan Manajemen, diantaranya adalah http://ejournal.stikessalsabilaserang.ac.id/,&nbsp; http://ejurnal.akperbinainsan.ac.id/index.php/JSS/about/editorialTeam, https://ejournal.akperrspadjakarta.ac.id/index.php/JEN/about/editorialTeam/index.php/JEN/about/editorialTeam, http://ejournal.akperfatmawati.ac.id/index.php/JIKO/about/editorialTeam&nbsp; &nbsp;\r\n</div>\r\n<div>\r\n&nbsp;\r\n</div>\r\n<div align=\"center\">\r\n<table border=\"1\" cellpadding=\"0\" width=\"712\" class=\"MsoNormalTable\" style=\"width: 533.75pt; background: #f9f9f9; border: 1pt solid #aaaaaa\">\r\n	<tbody>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>Nama</span></strong><strong></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span>Assoc. Prof. Dr. Al-Bahra, S.Kom, M.Kom.</span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>Tempat Tanggal Lahir</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span class=\"nickname\"><span>Jakarta, 2 Maret 1971</span></span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>NIDN</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span class=\"nickname\"><span>0403127401</span></span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>Nomor\r\n			Sertifikat Pendidik</span><span></span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span>081167511496</span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>NIRA (Nomor\r\n			Induk Registrasi Asesor)</span></strong><strong><span></span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span>9908116751149603827</span><span class=\"nickname\"><span></span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>ID Sinta</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<span>6714269</span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>ID Scopus</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<span class=\"nickname\"><span>99999999</span></span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>Jabatan Fungsional Dosen</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span class=\"nickname\"><span>Associate\r\n			Professor</span></span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>&nbsp;</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span class=\"nickname\"><span>(Lektor\r\n			Kepala, 820 kum, TMT : 01 Februari 2008)</span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>Alamat Kantor</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span class=\"nickname\"><span>Jl.\r\n			Jend. Sudirman No, 40 Modern &ndash; Cikokol Tangerang 15117, Indonesia</span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>Bidang Ilmu </span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span class=\"nickname\"><span>Teknologi\r\n			Pendidikan dan Teknologi Informasi</span></span><span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>e-mail akademis</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<span><a href=\"mailto:albahra@raharja.info\">albahra@raharja.info</a></span><span class=\"nickname\"><span></span></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td width=\"293\" valign=\"top\" style=\"width: 220.05pt; border: none; padding: 2.4pt\">\r\n			<p style=\"text-align: justify\" class=\"MsoNormal\">\r\n			<strong><span>e-mail tanya jawab agama Islam</span></strong>\r\n			</p>\r\n			</td>\r\n			<td width=\"412\" valign=\"top\" style=\"width: 309.2pt; border: none; padding: 2.4pt\">\r\n			<p class=\"MsoNormal\">\r\n			<a href=\"mailto:ustalbahra@gmail.com\">ustalbahra@gmail.com</a>\r\n			<span class=\"nickname\"></span>\r\n			</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n<div>\r\n&nbsp;\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\n&nbsp;\r\n</div>\r\n', '', 'Sabtu', '2020-04-25', '06:03:11', '73bahra.jpg', 0, '');
INSERT INTO `berita` VALUES (664, 57, 'admin', 'PROFIL TK AL-HIKMAH', '', '', 'profil-tk-alhikmah', 'N', 'N', 'N', 'Yayasan Al-Hikmah Citra Raya sebagai lembaga pendidikan yang senantiasa berusaha ikut serta dalam rangka mensukseskan tujuan pendidikan Nasional Indonesia. Sejak berdirinya Yayasan Al-Hikmah Citra Raya ini sudah bertekad untuk bergerak dalam bidang Pendidikan, Da&rsquo;wah &amp; Sosail. Yayasan Al-Hikmah Citra Raya berusaha untuk memberikan pelayanan yang baik kepada masyarakat setempat dalam upaya peningkatan mutu pendidikan. Salah satu upaya pengembangan pendidikan yang dilakukan adalah dengan membuka dan menyelenggarakan Taman Kanak-Kanak (TK). TK Al-Hikmah dalam kiprahnya di dunia pendidikan terutama dalam rangka penyelenggaraan TK perlu mendapatkan dukungan dan dorongan dari berbagai pihak yang berkompeten. Dukungan dan dorongan tersebut terutama yang diharapkan dari Dinas P dan K sebagai Instansi yang berwenang atas penyelenggaraan pendidikan di seantero Nasional Indonesia\r\n', '', 'Jumat', '2020-04-24', '15:17:08', '28DSC_0027.JPG', 0, '');
INSERT INTO `berita` VALUES (665, 57, 'admin', 'Prosedur Penilaian TK Al-Hikmah', '', '', 'prosedur-penilaian-tk-alhikmah', 'N', 'N', 'N', '<div align=\"justify\">\r\nGuru melaksanakan penilaian dengan mengacu pada kemampuan (indikator) yang hendak dicapai dalam satu satuan kegiatan yang direncanakan dalam tahapan waktu tertentu dengan memperhatikan prinsip penilaian yang telah ditentukan. Penilaian dilakukan seiring dengan kegiatan pembelajaran. Guru tidak secara khusus melaksanakan penilaian, tetapi ketika pembelajaran dan kegiatan bermain berlangsung, guru dapat sekaligus melaksanakan penilaian. Dalam pelaksanaan penilaian sehari-hari, guru menilai kemampuan (indikator) semua anak yang hendak dicapai seperti yang telah diprogramkan dalam satuan kegiatan harian (SKH)\r\n</div>\r\n', '', 'Jumat', '2020-04-24', '15:20:22', '9Model-Implementasi-Penilaian-Autentik-626x391.jpg', 0, '');
INSERT INTO `berita` VALUES (666, 48, 'admin', 'Agenda Ramadhan', '', '', 'agenda-ramadhan', 'N', 'N', 'N', '<div>\r\nYayasan Al-Hikmah Citra Raya&nbsp; &nbsp;mengucapkan :&nbsp;\r\n</div>\r\n<div>\r\nSelamat Menunaikan Ibadah Shaum 1441H\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\nAGENDA ROMADHON 1441H YAYASAN AL-HIKMAH CITRA RAYA\r\n</div>\r\n<div>\r\nJl.Lestari-21 Blok J11/23 Graha Lestari Citra Raya-Tangerang\r\n</div>\r\n<div>\r\n1. Pesantren Romadhon\r\n</div>\r\n<div>\r\n2. Pemutaran Film-Film Islami\r\n</div>\r\n<div>\r\n3. Ifthor Bersama\r\n</div>\r\n<div>\r\n4. Tadarrus (Tilawah Al-Qur&rsquo;an)\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n', '', 'Sabtu', '2020-04-25', '05:56:18', '61logo2.jpg', 0, '');
INSERT INTO `berita` VALUES (662, 56, 'admin', 'Tingkatkan Kualitas, Kepala PAUD Al-Hikmah Ikut Diklat Online', '', '', 'tingkatkan-kualitas-kepala-paud-alhikmah-ikut-diklat-online', 'N', 'N', 'N', '<p style=\"box-sizing: border-box; margin: 0px; padding: 0px 0px 1em; border: 0px none; outline: currentcolor none 0px; background: #ffffff none repeat scroll 0px 0px; font-size: 14px; vertical-align: baseline; color: #666666; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 500; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial\">\r\nUntuk meningkatkan kualitas dan menjadi tenaga profesional, Kepala PAUD Al-Hikmah&nbsp; Bengkulu mengikuti diklat online #DariRumahSaja, (Rabu, 22/4). Diklat yang membahas tentang &ldquo;Konsep Pembelajaran Berorientasi HOTS&rdquo;&nbsp; terselenggara atas kerjasama Diknas Provinsi Banten dan IGTKI Banten.\r\n</p>\r\n<p style=\"box-sizing: border-box; margin: 0px; padding: 0px 0px 1em; border: 0px none; outline: currentcolor none 0px; background: #ffffff none repeat scroll 0px 0px; font-size: 14px; vertical-align: baseline; color: #666666; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 500; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial\">\r\nKegiatan ini dibuka oleh Plt Diknas Provinsi banten Eri Yulian Hidayat dan dipandu oleh Pengawas Ibu Lesmi Hartati. Melalui aplikasi online, 46 kepala sekolah mengikuti diklat mulai pukul 09.00 WIB &ndash; 12.00 WIB. Jadi, meski dalam suasana pandemi Covid-19 dan<span>&nbsp;</span><em style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px none; outline: currentcolor none 0px; background: rgba(0, 0, 0, 0) none repeat scroll 0px 0px; font-size: 14px; vertical-align: baseline; font-style: italic\">work from home</em><span>&nbsp;</span>(WFH), guru PAUD Al-Hikmah tetap belajar untuk meningkatkan kompetensinya.\r\n</p>\r\n', '', 'Jumat', '2020-04-24', '15:07:08', '79DSCN4369.jpg', 0, '');
INSERT INTO `berita` VALUES (663, 56, 'admin', 'PROFIL PAUD AL-HIKMAH', '', '', 'profil-paud-alhikmah', 'N', 'N', 'N', 'Yayasan Al-Hikmah Citra Raya sebagai lembaga pendidikan yang senantiasa berusaha ikut serta dalam rangka mensukseskan tujuan pendidikan Nasional Indonesia. Sejak berdirinya Yayasan Al-Hikmah Citra Raya ini sudah bertekad untuk bergerak dalam bidang Pendidikan, Da&rsquo;wah &amp; Sosail. Yayasan Al-Hikmah Citra Raya berusaha untuk memberikan pelayanan yang baik kepada masyarakat setempat dalam upaya peningkatan mutu pendidikan. Salah satu upaya pengembangan pendidikan yang dilakukan adalah dengan membuka dan menyelenggarakan Pendidikan Anak Usia Dini (PAUD). PAUD Al-Hikmah dalam kiprahnya di dunia pendidikan terutama dalam rangka penyelenggaraan PAUD perlu mendapatkan dukungan dan dorongan dari berbagai pihak yang berkompeten. Dukungan dan dorongan tersebut terutama yang diharapkan dari Dinas P dan K sebagai Instansi yang berwenang atas penyelenggaraan pendidikan di seantero Nasional Indonesia.<!--[if gte mso 9]><xml>\r\n<o:OfficeDocumentSettings>\r\n<o:TargetScreenSize>800x600</o:TargetScreenSize>\r\n</o:OfficeDocumentSettings>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n<w:WordDocument>\r\n<w:View>Normal</w:View>\r\n<w:Zoom>0</w:Zoom>\r\n<w:TrackMoves/>\r\n<w:TrackFormatting/>\r\n<w:PunctuationKerning/>\r\n<w:ValidateAgainstSchemas/>\r\n<w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n<w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n<w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n<w:DoNotPromoteQF/>\r\n<w:LidThemeOther>EN-US</w:LidThemeOther>\r\n<w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n<w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n<w:Compatibility>\r\n<w:BreakWrappedTables/>\r\n<w:SnapToGridInCell/>\r\n<w:WrapTextWithPunct/>\r\n<w:UseAsianBreakRules/>\r\n<w:DontGrowAutofit/>\r\n<w:SplitPgBreakAndParaMark/>\r\n<w:EnableOpenTypeKerning/>\r\n<w:DontFlipMirrorIndents/>\r\n<w:OverrideTableStyleHps/>\r\n</w:Compatibility>\r\n<w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>\r\n<m:mathPr>\r\n<m:mathFont m:val=\"Cambria Math\"/>\r\n<m:brkBin m:val=\"before\"/>\r\n<m:brkBinSub m:val=\"&#45;-\"/>\r\n<m:smallFrac m:val=\"off\"/>\r\n<m:dispDef/>\r\n<m:lMargin m:val=\"0\"/>\r\n<m:rMargin m:val=\"0\"/>\r\n<m:defJc m:val=\"centerGroup\"/>\r\n<m:wrapIndent m:val=\"1440\"/>\r\n<m:intLim m:val=\"subSup\"/>\r\n<m:naryLim m:val=\"undOvr\"/>\r\n</m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n<w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"false\"\r\nDefSemiHidden=\"false\" DefQFormat=\"false\" DefPriority=\"99\"\r\nLatentStyleCount=\"376\">\r\n<w:LsdException Locked=\"false\" Priority=\"0\" QFormat=\"true\" Name=\"Normal\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 7\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 8\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 9\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 5\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 6\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 7\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 8\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index 9\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 7\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 8\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"toc 9\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Normal Indent\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"footnote text\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"annotation text\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"header\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"footer\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"index heading\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"35\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"caption\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"table of figures\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"envelope address\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"envelope return\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"footnote reference\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"annotation reference\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"line number\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"page number\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"endnote reference\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"endnote text\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"table of authorities\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"macro\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"toa heading\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Bullet\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Number\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List 5\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Bullet 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Bullet 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Bullet 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Bullet 5\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Number 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Number 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Number 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Number 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"10\" QFormat=\"true\" Name=\"Title\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Closing\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Signature\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"0\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"Default Paragraph Font\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text Indent\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Continue\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Continue 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Continue 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Continue 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"List Continue 5\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Message Header\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"11\" QFormat=\"true\" Name=\"Subtitle\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Salutation\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Date\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text First Indent\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text First Indent 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Note Heading\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text Indent 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Body Text Indent 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Block Text\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Hyperlink\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"FollowedHyperlink\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"22\" QFormat=\"true\" Name=\"Strong\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"20\" QFormat=\"true\" Name=\"Emphasis\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Document Map\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Plain Text\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"E-mail Signature\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Top of Form\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Bottom of Form\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Normal (Web)\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Acronym\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Address\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Cite\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Code\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Definition\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Keyboard\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Preformatted\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Sample\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Typewriter\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"HTML Variable\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Normal Table\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"annotation subject\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"No List\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Outline List 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Outline List 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Outline List 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Simple 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Simple 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Simple 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Classic 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Classic 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Classic 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Classic 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Colorful 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Colorful 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Colorful 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Columns 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Columns 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Columns 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Columns 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Columns 5\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 5\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 6\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 7\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Grid 8\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 4\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 5\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 6\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 7\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table List 8\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table 3D effects 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table 3D effects 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table 3D effects 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Contemporary\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Elegant\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Professional\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Subtle 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Subtle 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Web 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Web 2\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Web 3\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Balloon Text\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" Name=\"Table Grid\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Table Theme\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Placeholder Text\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"1\" QFormat=\"true\" Name=\"No Spacing\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Revision\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"34\" QFormat=\"true\"\r\nName=\"List Paragraph\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"29\" QFormat=\"true\" Name=\"Quote\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"30\" QFormat=\"true\"\r\nName=\"Intense Quote\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"19\" QFormat=\"true\"\r\nName=\"Subtle Emphasis\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"21\" QFormat=\"true\"\r\nName=\"Intense Emphasis\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"31\" QFormat=\"true\"\r\nName=\"Subtle Reference\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"32\" QFormat=\"true\"\r\nName=\"Intense Reference\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"33\" QFormat=\"true\" Name=\"Book Title\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"37\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" Name=\"Bibliography\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\nUnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"TOC Heading\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"41\" Name=\"Plain Table 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"42\" Name=\"Plain Table 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"43\" Name=\"Plain Table 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"44\" Name=\"Plain Table 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"45\" Name=\"Plain Table 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"40\" Name=\"Grid Table Light\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\" Name=\"Grid Table 1 Light\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\" Name=\"Grid Table 6 Colorful\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\" Name=\"Grid Table 7 Colorful\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"Grid Table 1 Light Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"Grid Table 6 Colorful Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"Grid Table 7 Colorful Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"Grid Table 1 Light Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"Grid Table 6 Colorful Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"Grid Table 7 Colorful Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"Grid Table 1 Light Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"Grid Table 6 Colorful Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"Grid Table 7 Colorful Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"Grid Table 1 Light Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"Grid Table 6 Colorful Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"Grid Table 7 Colorful Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"Grid Table 1 Light Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"Grid Table 6 Colorful Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"Grid Table 7 Colorful Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"Grid Table 1 Light Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"Grid Table 6 Colorful Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"Grid Table 7 Colorful Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\" Name=\"List Table 1 Light\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\" Name=\"List Table 6 Colorful\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\" Name=\"List Table 7 Colorful\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"List Table 1 Light Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"List Table 6 Colorful Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"List Table 7 Colorful Accent 1\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"List Table 1 Light Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"List Table 6 Colorful Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"List Table 7 Colorful Accent 2\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"List Table 1 Light Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"List Table 6 Colorful Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"List Table 7 Colorful Accent 3\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"List Table 1 Light Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"List Table 6 Colorful Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"List Table 7 Colorful Accent 4\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"List Table 1 Light Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"List Table 6 Colorful Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"List Table 7 Colorful Accent 5\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"46\"\r\nName=\"List Table 1 Light Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"51\"\r\nName=\"List Table 6 Colorful Accent 6\"/>\r\n<w:LsdException Locked=\"false\" Priority=\"52\"\r\nName=\"List Table 7 Colorful Accent 6\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Mention\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Smart Hyperlink\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Hashtag\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Unresolved Mention\"/>\r\n<w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\nName=\"Smart Link\"/>\r\n</w:LatentStyles>\r\n</xml><![endif]--><!--[if gte mso 10]>\r\n<style>\r\n/* Style Definitions */\r\ntable.MsoNormalTable\r\n{mso-style-name:\"Table Normal\";\r\nmso-tstyle-rowband-size:0;\r\nmso-tstyle-colband-size:0;\r\nmso-style-noshow:yes;\r\nmso-style-priority:99;\r\nmso-style-parent:\"\";\r\nmso-padding-alt:0cm 5.4pt 0cm 5.4pt;\r\nmso-para-margin:0cm;\r\nmso-para-margin-bottom:.0001pt;\r\nmso-pagination:widow-orphan;\r\nfont-size:10.0pt;\r\nfont-family:\"Times New Roman\",serif;}\r\n</style>\r\n<![endif]-->\r\n', '', 'Jumat', '2020-04-24', '15:12:19', '40DSC_0023.JPG', 0, '');
INSERT INTO `berita` VALUES (661, 54, 'admin', 'Biografi ketua yayasan', '', '', 'biografi-ketua-yayasan', 'N', 'N', 'N', '<div align=\"justify\">\r\nAbdul Qohar lahir di Jakarta, 28 Januari 1996. Sarjana Pendidikan dari Universitas Islam Nusantara. Diploma Bahasa Arab dari Ma&rsquo;had Al-Imarot Kota Bandung. Saat ini sedang menempuh program peendidikan Magister Manajemen. Pengajar Bahasa Arab pada Pondok Pesantren Daarul Muqorrabon. <br />\r\n<br />\r\nPernah mondok di Pondok Pesantren Al-Amanah Boarding School dan Pondok Pesantren Darul Muqorrobin. Sudah menjadi tim penulis buku Pendidikan Agama Islam untuk Perguruan Tinggi. Aktif mengisi kajian-kajian keislaman seputar Aqidah, Akhlaq dan Fiqih, serta memberikan khutbah jum&rsquo;at dan khutbah idul fitri<br />\r\n<br />\r\n</div>\r\n', '', 'Jumat', '2020-04-24', '13:56:09', '', 0, '');

-- ----------------------------
-- Table structure for download
-- ----------------------------
DROP TABLE IF EXISTS `download`;
CREATE TABLE `download`  (
  `id_download` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nama_file` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  `hits` int(3) NOT NULL,
  PRIMARY KEY (`id_download`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of download
-- ----------------------------
INSERT INTO `download` VALUES (11, 'Adab Pergaulan dalam Islam', 'Adab Pergaulan dalam Islam-08122019.pdf', '2020-04-25', 13);
INSERT INTO `download` VALUES (12, 'Akhlaq-Bagian-1', 'Akhlaq-Bagian-1-Publish-24122019.pdf', '2020-04-25', 5);

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery`  (
  `id_gallery` int(5) NOT NULL AUTO_INCREMENT,
  `id_album` int(5) NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `jdl_gallery` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gallery_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gbr_gallery` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_gallery`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 248 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gallery
-- ----------------------------
INSERT INTO `gallery` VALUES (240, 30, 'admin', 'PAUD Al-Hikmah Citra Raya', 'paud-alhikmah', '', '2DSC_0023.JPG');
INSERT INTO `gallery` VALUES (246, 30, 'admin', 'TK Al-Hikmah Citra Raya', 'tk-alhikmah', '', '24IMG_1394.JPG');
INSERT INTO `gallery` VALUES (247, 30, 'admin', 'TK Al-Hikmah Citra Raya', 'tk-alhikmah', '', '65Lomba TK-PAUD Al-Hikmah di Pizza Citra Raya.jpeg');
INSERT INTO `gallery` VALUES (245, 30, 'admin', 'TK Al-Hikmah Citra Raya', 'tk-alhikmah', '', '36IMG_1383.JPG');
INSERT INTO `gallery` VALUES (243, 31, 'admin', 'PAUD Al-Hikmah Citra Raya', 'paud-alhikmah', '', '86PAUD Angkatan 2014-2015 -1.JPG');
INSERT INTO `gallery` VALUES (244, 30, 'admin', 'TK Al-Hikmah Citra Raya', 'tk-alhikmah', '', '75DSC_0027.JPG');
INSERT INTO `gallery` VALUES (241, 0, 'admin', 'PAUD Al-Hikmah Citra Raya', 'paud-alhikmah', '', '77PAUD Angkatan 2014-2015 -1.JPG');
INSERT INTO `gallery` VALUES (242, 0, 'admin', 'PAUD Al-Hikmah Citra Raya', 'paud-alhikmah', '', '24PAUD Angkatan 2014-2015 -1.JPG');

-- ----------------------------
-- Table structure for halamanstatis
-- ----------------------------
DROP TABLE IF EXISTS `halamanstatis`;
CREATE TABLE `halamanstatis`  (
  `id_halaman` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isi_halaman` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT 1,
  `jam` time NOT NULL,
  `hari` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_halaman`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of halamanstatis
-- ----------------------------
INSERT INTO `halamanstatis` VALUES (18, 'Tentang Kami', 'tentang-kami', '<div style=\"text-align: justify\">\r\nYayasan Al-Hikmah Citra Raya adalah suatu yayasan Islam yang dibentuk dan dikelola dari, oleh dan untuk masyarakat yang memfokuskan diri pada Pendidikan, Da&rsquo;wah, dan Sosial.\r\n</div>\r\n<div style=\"text-align: justify\">\r\nYayasan Al-Hikmah Citra Raya yang berlokasi di Cluster Graha Lestari Perumahan Citra Raya &ndash; Tangerang sudah berdiri sejak 01 Oktober 2009 (12 Syawal 1430H) dengan Akte Notaris No. 37 tanggal 28 September 2009, dengan Notaris Zainuddin Thohir,SH. Lalu di syahkan dengan Surat Keputusan Menteri Hukum &amp; HAM Negara Kesatuan&nbsp; Republik Indonesia No. AHU-4739 .A.H.01.04. Tahun 2009.&nbsp;\r\n</div>\r\n<div style=\"text-align: justify\">\r\nSampaikanlah walau satu ayat ! adalah moto dari yayasan Al-Hikmah Citra Raya. Moto yang sederhana tapi cukup untuk menjadi landasan gerak dan semangat dalam setiap aktivitas yang dilaksanakan oleh yayasan ini. Ada dua hal penting yang menjadi dasar dari moto dan semangat tersebut, yaitu:&nbsp;\r\n</div>\r\n<div style=\"text-align: justify\">\r\n<ol>\r\n	<li>Mensyiarkan dienul Islam kepada seluruh umat manusia dengan berupaya menjadi uswah dalam mewujudkan masyarakat&nbsp; yang beriman dan bertaqwa, seperti firman Allah dalam Al-Qur&rsquo;an surat Al-A&rsquo;raf ayat 96 yang artinya :&rdquo;Jikalau sekiranya penduduk negeri-negeri beriman dan bertakwa, pastilah Kami akan melimpahkan kepada mereka berkah dari langit dan bumi,&nbsp; tetapi mereka mendustakan&nbsp; (ayat-ayat Kami) itu, Maka Kami siksa mereka disebabkan perbuatannya&rdquo;(QS:Al-A&rsquo;raf:96).</li>\r\n	<li>Terbentuknya Masyarakat Yang Memahami Tujuan Hidup, Pedoman Hidup dan Ikhlas dalam beramal, Seperti Firman Allah Dalam Al-Qur&rsquo;an Surat Adz-Dzariyaat ayat 56 yang artinya : &rdquo;Dan aku tidak menciptakan jin dan manusia melainkan supaya mereka mengabdi kepada-Ku&rdquo;.</li>\r\n</ol>\r\n</div>\r\n<div style=\"text-align: justify\">\r\nSementara itu, kualitas sumber daya manusia berkaitan erat dengan pendidikan, seperti salah satu hadits Rasulullah Muhammad SAW yang artinya &rdquo; Tuntutlah Ilmu walau ke Negeri Cina&rdquo;. Islam sangat menganjurkan ummatnya untuk selalu menuntut ilmu, walaupun ke tempat yang sangat jauh sekalipun. Yayasan Al-Hikmah Citra Raya hadir untuk mengajak masyarakat memahami tujuan serta tauladan hidup mereka melalui pendidikan dan mendidik masyarakat untuk selalu ikhlas dalam beramal melalui aktivitas sosial di tengah masyarakat. Kesejahteraan masyarakat hanya akan terwujud dan terlaksana dengan baik, hanyalah dengan peningkatan dan pemerataan pendidikan bagi masyarakat, seperti pepatah, &rdquo;berilah kail untuk memancing, bukan ikan&rdquo;. Maka untuk meningkatkan taraf hidup masyarakat, haruslah dilakukan dengan meningkatkan pendidikan, karena dengan pendidikan mereka akan sadar untuk memahami asal-usul mereka, tujuan mereka hidup di alam dunia ini, dan akan kemana akhir dari kehidupan mereka kelak, serta bagaimana mereka harus menjalani kehidupan sementara dialam dunia ini, agar dapat selamat kelak di kehidupan yang abadi.\r\n</div>\r\n<div style=\"text-align: justify\">\r\nDalam mencapai berbagai tujuan tersebut, yayasan Al-Hikmah Citra Raya akan membangun sinergi dan kerja sama dengan berbagai pihak, baik pemerintah, perusahaan, juga masyarakat pada umumnya. Semoga yayasan Al-Hikmah Citra Raya dapat selalu berada ditengah masyarakat dalam memberikan pendidikan, dan da&rsquo;wah untuk semua serta selalu hadir dalam setiap kegiatan sosial dimasyarakat dengan penuh keikhlasan dan sepenuh hati/nafsiyah.\r\n</div>\r\n<div style=\"text-align: justify\">\r\n<br />\r\n</div>\r\n', '2012-03-08', '61logo2.jpg', 'admin', 733, '20:08:00', 'Kamis');
INSERT INTO `halamanstatis` VALUES (28, 'PAUD Al-Hikmah', 'paud-alhikman', '<div style=\"text-align: justify\">\r\nYayasan Al-Hikmah Citra Raya sebagai lembaga pendidikan yang senantiasa berusaha ikut serta dalam rangka mensukseskan tujuan pendidikan Nasional Indonesia. Sejak berdirinya Yayasan Al-Hikmah Citra Raya ini sudah bertekad untuk bergerak dalam bidang Pendidikan, Da&rsquo;wah &amp; Sosail. Yayasan Al-Hikmah Citra Raya berusaha untuk memberikan pelayanan yang baik kepada masyarakat setempat dalam upaya peningkatan mutu pendidikan. Salah satu upaya pengembangan pendidikan yang dilakukan adalah dengan membuka dan menyelenggarakan Pendidikan Anak Usia Dini (PAUD). PAUD Al-Hikmah dalam kiprahnya di dunia pendidikan terutama dalam rangka penyelenggaraan PAUD perlu mendapatkan dukungan dan dorongan dari berbagai pihak yang berkompeten. Dukungan dan dorongan tersebut terutama yang diharapkan dari Dinas P dan K sebagai Instansi yang berwenang atas penyelenggaraan pendidikan di seantero Nasional Indonesia\r\n</div>\r\n', '2020-04-25', '51PAUD Angkatan 2013-2014.JPG', 'admin', 0, '05:24:56', 'Sabtu');
INSERT INTO `halamanstatis` VALUES (29, 'TK Al-Hikmah', 'tk-alhikmah', '<div style=\"text-align: justify\">\r\nYayasan Al-Hikmah Citra Raya sebagai lembaga pendidikan yang senantiasa berusaha ikut serta dalam rangka mensukseskan tujuan pendidikan Nasional Indonesia. Sejak berdirinya Yayasan Al-Hikmah Citra Raya ini sudah bertekad untuk bergerak dalam bidang Pendidikan, Da&rsquo;wah &amp; Sosail. Yayasan Al-Hikmah Citra Raya berusaha untuk memberikan pelayanan yang baik kepada masyarakat setempat dalam upaya peningkatan mutu pendidikan. Salah satu upaya pengembangan pendidikan yang dilakukan adalah dengan membuka dan menyelenggarakan Taman Kanak-Kanak (TK). TK Al-Hikmah dalam kiprahnya di dunia pendidikan terutama dalam rangka penyelenggaraan TK perlu mendapatkan dukungan dan dorongan dari berbagai pihak yang berkompeten. Dukungan dan dorongan tersebut terutama yang diharapkan dari Dinas P dan K sebagai Instansi yang berwenang atas penyelenggaraan pendidikan di seantero Nasional Indonesia\r\n</div>\r\n', '2020-04-25', '51DSC_0027.JPG', 'admin', 0, '05:41:42', 'Sabtu');
INSERT INTO `halamanstatis` VALUES (30, 'TPA Al-Hikmah', 'tpa-alhikmah', '<div>\r\nYayasan Al-Hikmah Citra Raya mengundang ibu-ibu untuk menghadiri Pengajian Bulanan Wali Murid TPA Al-Hikmah (Baik yang masih aktif dan tidak aktif lagi), pada :&nbsp;\r\n</div>\r\n<div>\r\nHari/Tgl<span style=\"white-space: pre\">		</span>: Ahad, 31 Oktober 2010\r\n</div>\r\n<div>\r\nWaktu<span style=\"white-space: pre\">		</span>: 18.30 &ndash; 20.30 (Ba&rsquo;da Maghrib)\r\n</div>\r\n<div>\r\nTempat<span style=\"white-space: pre\">		</span>: TPA Al-Hikmah (J08/74)\r\n</div>\r\n<div>\r\nAcara<span style=\"white-space: pre\">		</span>: Yasin-an &amp; Tausiyah oleh Ustzh. Hj. Nuroniyah\r\n</div>\r\n<div>\r\n&nbsp;\r\n</div>\r\n<div>\r\nWassalamu&rsquo;alaikum Warahmatullahi Wabaraktuh\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n', '2020-04-25', '61IMG_1384.JPG', 'admin', 0, '05:43:53', 'Sabtu');
INSERT INTO `halamanstatis` VALUES (31, 'Dakwah', 'dakwah', '<div>\r\n&ldquo;Jikalau Sekiranya penduduk negeri-negeri beriman dan bertakwa, pastilah Kami akan melimpahkan kepada mereka berkah dari langit dan bumi, tetapi mereka mendustakan (ayat-ayat Kami) itu, Maka Kami siksa mereka disebabkan perbuatannya&rdquo;. (QS : Al-A&rsquo;raf [7] : 96)\r\n</div>\r\n<div>\r\nAqidah merupakan awal terbentunya generasi bangsa yang menentukan masa depan bangsa selanjutnya. Dari usia lahir sampai dengan memasuki masa tua merupakan masa keemasan sekaligus masa kritis dalam tahapan kehidupan manusia, yang akan menentukan seperti apa akhir hidupnya kelak. Majlis Ta&rsquo;lim meruapakan salah satu sarana untuk selalu membina dan mempertahankan &lsquo;aqidah sesorang dalam mengarungi bahtera kehidupan di alam dunia ini.&nbsp;\r\n</div>\r\n<div>\r\nPendidikan yang salah pada masa usia dini akan berdampak negative terhadap perkembangan Aqidah dimasa depan. Riset dibidang Neurologi (Osborn, White Bloom) juga mebuktikan bahwa kecerdasan Aqidah yang saling terhubungkan. Dalam hal ini peranan rangsangan yang diberikan sejak dini akan sangat berpengaruh terhadap proses penghubungan dan penguatan sel-sel dan simpul &ndash;simpul syaraf otak tersebut. Lebih lanjut hadirnya teori baru tentang multiple intelligences juga mengingatkan kepada kita bahwa setiap Aqidah memiliki beberapa potensi kecerdasan; pada empat tahu pertama separuh kapasitas kecerdasan manusia sudah terbentuk, artinya pada usia tersebut otak Aqidah tidak mendapatkan rangsangan yang optimal, maka potensi otak Aqidah tidak akan akan berkembang secara maksimal. Dan potensi kecerdasan tersebut akan berkembang secara optimal bila dikembangkan sejak dini melalui layanan pendidikan yang tepat dan sesuai dengan tingkat perkembangan Aqidah.&nbsp;\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n', '2020-04-25', '7logo2.jpg', 'admin', 0, '05:46:47', 'Sabtu');
INSERT INTO `halamanstatis` VALUES (32, 'Sosial', 'sosial', '<div style=\"text-align: justify\">\r\n<font face=\"Arial, sans-serif\">Yayasan Al-Hikmah Citra Raya yang berlokasi di Cluster Graha Lestari Perumahan Citra Raya &ndash; Tangerang sudah berdiri sejak 01 Oktober 2009 (12 Syawal 1430H) dengan Akte Notaris No. 37 tanggal 28 September 2009, dengan Notaris Zainuddin Thohir,SH. Lalu di syahkan dengan Surat Keputusan Menteri Hukum &amp; HAM Negara Kesatuan&nbsp; Republik Indonesia No. AHU-4739 .A.H.01.04. Tahun 2009. Serta Tanda Daftar Dinas Sosial : No.464.52.7.2010.&nbsp;</font>\r\n</div>\r\n<div style=\"text-align: justify\">\r\n<font face=\"Arial, sans-serif\">Yayasan Al-Hikmah Citra Raya tersebut berdiri dengan harapan agar dapat menjadi salah satu yayasan Islam yang dapat melayani masyarakat di lingkungan Kabupaten Tangerang, Kota Tangerang, dan Tangerang Selatan&nbsp; khususnya dan Provinsi Banten umumnya. Yayasan Al-Hikmah Citra Raya bergerak dalam dibidang Pendidikan, Da&rsquo;wah &amp; Sosial</font>\r\n</div>\r\n', '2020-04-25', '19logo2.jpg', 'admin', 0, '05:53:26', 'Sabtu');

-- ----------------------------
-- Table structure for header
-- ----------------------------
DROP TABLE IF EXISTS `header`;
CREATE TABLE `header`  (
  `id_header` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  PRIMARY KEY (`id_header`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 34 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of header
-- ----------------------------
INSERT INTO `header` VALUES (31, 'Header3', '', 'header3.jpg', '2011-04-06');
INSERT INTO `header` VALUES (32, 'Header2', '', 'header1.jpg', '2011-04-06');
INSERT INTO `header` VALUES (33, 'Header1', '', 'header2.jpg', '2011-04-06');

-- ----------------------------
-- Table structure for hubungi
-- ----------------------------
DROP TABLE IF EXISTS `hubungi`;
CREATE TABLE `hubungi`  (
  `id_hubungi` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `subjek` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `pesan` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `dibaca` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_hubungi`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hubungi
-- ----------------------------
INSERT INTO `hubungi` VALUES (34, 'tes', 'tes@yahoo.com', 'tess', 'tess', '2020-04-25', '00:00:00', 'Y');

-- ----------------------------
-- Table structure for identitas
-- ----------------------------
DROP TABLE IF EXISTS `identitas`;
CREATE TABLE `identitas`  (
  `id_identitas` int(5) NOT NULL AUTO_INCREMENT,
  `nama_website` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `facebook` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `rekening` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telp` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `meta_deskripsi` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `meta_keyword` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `favicon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_identitas`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of identitas
-- ----------------------------
INSERT INTO `identitas` VALUES (1, 'Yayasan Al-Hikmah Citra Raya', 'hudzaifah.alba@yahoo.com', 'http://alhikmahcitraraya.or.id/', '', '', '021-98496109', 'Yayasan Al-Hikmah Citra Raya', 'alhikmah, citraraya', 'favicon.png', 'Jl. Lestari No 21 Blok J-11/23 RT. 04/08, Graha Lestari Citra Raya Tangerang');

-- ----------------------------
-- Table structure for iklanatas
-- ----------------------------
DROP TABLE IF EXISTS `iklanatas`;
CREATE TABLE `iklanatas`  (
  `id_iklanatas` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  PRIMARY KEY (`id_iklanatas`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iklanatas
-- ----------------------------
INSERT INTO `iklanatas` VALUES (35, '', 'admin', '#', 'promoiklan.gif', '2012-08-12');

-- ----------------------------
-- Table structure for iklantengah
-- ----------------------------
DROP TABLE IF EXISTS `iklantengah`;
CREATE TABLE `iklantengah`  (
  `id_iklantengah` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  PRIMARY KEY (`id_iklantengah`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 32 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iklantengah
-- ----------------------------
INSERT INTO `iklantengah` VALUES (26, 'Lembaga Amil Zakat dan Pemberdayaan  DOMPET SOSIAL INSAN MULIA', '', '#', 'dsim_lbanner.jpg', '2011-06-26');
INSERT INTO `iklantengah` VALUES (30, 'ingin punya website?', '', 'hal-jasa-pembuatan-website.html', 'webDesignBanner.jpg', '2012-01-08');
INSERT INTO `iklantengah` VALUES (31, 'Contoh Iklan', '', '#', '179748contoh_iklan002.jpg', '2012-03-10');

-- ----------------------------
-- Table structure for katajelek
-- ----------------------------
DROP TABLE IF EXISTS `katajelek`;
CREATE TABLE `katajelek`  (
  `id_jelek` int(11) NOT NULL AUTO_INCREMENT,
  `kata` varchar(60) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `ganti` varchar(60) CHARACTER SET latin1 COLLATE latin1_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_jelek`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of katajelek
-- ----------------------------
INSERT INTO `katajelek` VALUES (4, 'sex', '', 's**');
INSERT INTO `katajelek` VALUES (2, 'bajingan', '', 'b*******');
INSERT INTO `katajelek` VALUES (3, 'bangsat', '', 'b******');

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori`  (
  `id_kategori` int(5) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `kategori_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 59 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES (57, 'TK AL-HIKMAH', 'admin', 'tk-alhikmah', 'Y');
INSERT INTO `kategori` VALUES (56, 'PAUD AL-HIKMAH', 'admin', 'paud-alhikmah', 'Y');
INSERT INTO `kategori` VALUES (55, 'biografi', '', 'biografi', 'Y');
INSERT INTO `kategori` VALUES (36, 'Pendidikan', '', 'komunitas', 'Y');
INSERT INTO `kategori` VALUES (39, 'Internasional', '', 'internasional', 'Y');
INSERT INTO `kategori` VALUES (54, 'lainnya', 'admin', 'lainnya', 'Y');
INSERT INTO `kategori` VALUES (42, 'Dunia Islam', '', 'dunia-islam', 'Y');
INSERT INTO `kategori` VALUES (44, 'Kegiatan', '', 'kegiatan', 'Y');
INSERT INTO `kategori` VALUES (46, 'Artikel Pendidikan', '', 'hasil-kajian', 'Y');
INSERT INTO `kategori` VALUES (47, 'Artikel Islam', '', 'rekomendasi', 'Y');
INSERT INTO `kategori` VALUES (48, 'sosial', '', 'sosial', 'Y');
INSERT INTO `kategori` VALUES (52, 'dakwah', 'admin', 'dakwah', 'Y');
INSERT INTO `kategori` VALUES (53, 'Sambutan-Sambutan', 'admin', 'sambutansambutan', 'Y');
INSERT INTO `kategori` VALUES (58, 'TPA Al-Hikmah', 'admin', 'tpa-alhikmah', 'Y');

-- ----------------------------
-- Table structure for komentar
-- ----------------------------
DROP TABLE IF EXISTS `komentar`;
CREATE TABLE `komentar`  (
  `id_komentar` int(5) NOT NULL AUTO_INCREMENT,
  `id_berita` int(5) NOT NULL,
  `nama_komentar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `isi_komentar` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl` date NOT NULL,
  `jam_komentar` time NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_komentar`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 138 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of komentar
-- ----------------------------
INSERT INTO `komentar` VALUES (84, 622, 'asyiqah', '', ' 	nice  story	   ', '2012-01-05', '00:15:45', 'Y', '');
INSERT INTO `komentar` VALUES (88, 633, 'belajarkonseling', 'www.belajarkonseling.com', ' hm...  kae.x  perlu  juga  ne  bantuan  dari  para  konselor...:)		   ', '2012-01-13', '20:09:07', 'Y', '');
INSERT INTO `komentar` VALUES (90, 597, 'Rizal Faizal', 'www.rizal-online.co.cc', ' asyik  aja  dehh...   ', '2012-02-25', '15:01:40', 'Y', '');
INSERT INTO `komentar` VALUES (91, 623, 'ridha', 'komputerkampus.com', ' makin  parah  aja  nih  ...\r\nmudah2n  bisa  berbenah  negeri  ku  yg  q  banggakan   ', '2012-03-08', '20:06:07', 'Y', '');
INSERT INTO `komentar` VALUES (136, 643, 'belajarweb', '', ' cantik  euy   ', '2013-01-19', '18:51:58', 'Y', '');
INSERT INTO `komentar` VALUES (137, 603, 'lukman', '', ' saya  yakin  PHP  juga  bisa  bertahan  sampai  2030   ', '2013-01-19', '18:56:25', 'Y', '');

-- ----------------------------
-- Table structure for komentarvid
-- ----------------------------
DROP TABLE IF EXISTS `komentarvid`;
CREATE TABLE `komentarvid`  (
  `id_komentar` int(5) NOT NULL AUTO_INCREMENT,
  `id_video` int(5) NOT NULL,
  `nama_komentar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `isi_komentar` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl` date NOT NULL,
  `jam_komentar` time NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_komentar`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 107 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for logo
-- ----------------------------
DROP TABLE IF EXISTS `logo`;
CREATE TABLE `logo`  (
  `id_logo` int(5) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_logo`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logo
-- ----------------------------
INSERT INTO `logo` VALUES (15, 'logo1.jpg');

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member`  (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_member`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id_menu` int(5) NOT NULL AUTO_INCREMENT,
  `id_parent` int(5) NOT NULL DEFAULT 0,
  `nama_menu` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `link` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `aktif` enum('Ya','Tidak') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Ya',
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 41 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (9, 8, 'Hukum', 'kategori-48-hukum.html', 'Ya');
INSERT INTO `menu` VALUES (8, 0, 'Nasional', '#', 'Ya');
INSERT INTO `menu` VALUES (7, 0, 'Home', 'index.php', 'Ya');
INSERT INTO `menu` VALUES (11, 8, 'Politik', 'kategori-22-politik.html', 'Ya');
INSERT INTO `menu` VALUES (12, 8, 'Ekonomi', 'kategori-21-ekonomi.html', 'Ya');
INSERT INTO `menu` VALUES (13, 0, 'Internasional', 'kategori-39-internasional.html', 'Ya');
INSERT INTO `menu` VALUES (14, 0, 'Teknologi', 'kategori-19-teknologi.html', 'Ya');
INSERT INTO `menu` VALUES (18, 0, 'Olahraga', 'kategori-2-olahraga.html', 'Ya');
INSERT INTO `menu` VALUES (19, 0, 'Hiburan', 'kategori-23-hiburan.html', 'Ya');
INSERT INTO `menu` VALUES (20, 0, 'Metropolitan', 'kategori-41-metropolitan.html', 'Ya');
INSERT INTO `menu` VALUES (21, 0, 'Dunia Islam', 'kategori-42-dunia-islam.html', 'Ya');
INSERT INTO `menu` VALUES (22, 39, 'Kuliner', 'kategori-40-kuliner.html', 'Ya');
INSERT INTO `menu` VALUES (23, 0, 'Video', 'semua-playlist.html', 'Ya');
INSERT INTO `menu` VALUES (40, 39, 'Kesehatan', 'kategori-31-kesehatan.html', 'Ya');
INSERT INTO `menu` VALUES (39, 0, '+ Lainnya', '', 'Ya');

-- ----------------------------
-- Table structure for mod_alamat
-- ----------------------------
DROP TABLE IF EXISTS `mod_alamat`;
CREATE TABLE `mod_alamat`  (
  `id_alamat` int(5) NOT NULL AUTO_INCREMENT,
  `alamat` varchar(250) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_alamat`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mod_alamat
-- ----------------------------
INSERT INTO `mod_alamat` VALUES (1, '<p>\r\nJl. Kalibata Selatan II/2B\r\n</p>\r\n<p>\r\nJakarta 12740 \r\n</p>\r\n<p>\r\nIndonesia \r\n</p>\r\n<p>\r\nTelp. 021.32972759\r\n</p>\r\n<p>\r\nEmail: <a href=\"mailto:rizal_fzl@yahoo.com\">rizal_fzl@yahoo.com</a> \r\n</p>\r\n');

-- ----------------------------
-- Table structure for mod_ym
-- ----------------------------
DROP TABLE IF EXISTS `mod_ym`;
CREATE TABLE `mod_ym`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mod_ym
-- ----------------------------
INSERT INTO `mod_ym` VALUES (1, 'Rizal Faizal', 'rizal_fzl');

-- ----------------------------
-- Table structure for modul
-- ----------------------------
DROP TABLE IF EXISTS `modul`;
CREATE TABLE `modul`  (
  `id_modul` int(5) NOT NULL AUTO_INCREMENT,
  `nama_modul` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `link` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `static_content` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `publish` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `status` enum('user','admin') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `urutan` int(5) NOT NULL,
  `link_seo` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_modul`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 71 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of modul
-- ----------------------------
INSERT INTO `modul` VALUES (2, 'Manajemen User', '', '?module=user', '', '', 'Y', 'user', 'Y', 22, '');
INSERT INTO `modul` VALUES (18, ' Berita', '', '?module=berita', '', '', 'Y', 'user', 'Y', 5, 'semua-berita.html');
INSERT INTO `modul` VALUES (19, 'Iklan Utama', '', '?module=banner', '', '', 'N', 'user', 'N', 9, '');
INSERT INTO `modul` VALUES (10, 'Manajemen Modul', '', '?module=modul', '', '', 'Y', 'user', 'Y', 23, '');
INSERT INTO `modul` VALUES (31, 'Kategori Berita ', '', '?module=kategori', '', '', 'Y', 'user', 'Y', 6, '');
INSERT INTO `modul` VALUES (33, 'Jajak Pendapat', '', '?module=poling', '', '', 'Y', 'user', 'Y', 18, '');
INSERT INTO `modul` VALUES (34, 'Tag Berita', '', '?module=tag', '', '', 'Y', 'user', 'Y', 7, '');
INSERT INTO `modul` VALUES (35, 'Komentar Berita', '', '?module=komentar', '', '', 'Y', 'user', 'Y', 8, '');
INSERT INTO `modul` VALUES (41, 'Agenda Jakarta', '', '?module=agenda', '', '', 'Y', 'user', 'Y', 17, 'semua-agenda.html');
INSERT INTO `modul` VALUES (43, 'Berita Foto', '', '?module=album', '', '', 'Y', 'user', 'Y', 11, '');
INSERT INTO `modul` VALUES (44, 'Galeri Berita Foto', '', '?module=galerifoto', '', '', 'Y', 'user', 'Y', 12, '');
INSERT INTO `modul` VALUES (45, 'Template Web', '', '?module=templates', '', '', 'Y', 'user', 'Y', 16, '');
INSERT INTO `modul` VALUES (46, 'Sensor Kata', '', '?module=katajelek', '', '', 'Y', 'user', 'Y', 10, '');
INSERT INTO `modul` VALUES (61, 'Identitas Website', '', '?module=identitas', '', '', 'Y', 'user', 'Y', 1, '');
INSERT INTO `modul` VALUES (57, 'Menu Utama', '', '?module=menuutama', '', '', 'Y', 'user', 'Y', 2, '');
INSERT INTO `modul` VALUES (58, 'Sub Menu', '', '?module=submenu', '', '', 'Y', 'user', 'Y', 3, '');
INSERT INTO `modul` VALUES (59, 'Halaman Baru', '', '?module=halamanstatis', '', '', 'Y', 'user', 'Y', 4, '');
INSERT INTO `modul` VALUES (62, 'Video', '', '?module=video', '', '', 'Y', 'user', 'Y', 13, '');
INSERT INTO `modul` VALUES (63, 'Playlist Video', '', '?module=playlist', '', '', 'Y', 'user', 'Y', 14, '');
INSERT INTO `modul` VALUES (64, 'Tag Video', '', '?module=tagvid', '', '', 'Y', 'user', 'Y', 15, '');
INSERT INTO `modul` VALUES (65, 'Komentar Video', '', '?module=komentarvid', '', '', 'Y', 'user', 'Y', 15, '');
INSERT INTO `modul` VALUES (66, 'Logo Website', '', '?module=logo', '', '', 'Y', 'user', 'Y', 15, '');
INSERT INTO `modul` VALUES (67, 'Iklan Layanan', '', '?module=iklanatas', '', '', 'Y', 'user', 'Y', 19, '');
INSERT INTO `modul` VALUES (68, 'Iklan Depan', '', '?module=iklantengah', '', '', 'Y', 'user', 'Y', 20, '');
INSERT INTO `modul` VALUES (69, 'Iklan Kiri', '', '?module=pasangiklan', '', '', 'Y', 'user', 'Y', 21, '');
INSERT INTO `modul` VALUES (70, 'Hubungi Kami', '', '?module=hubungi', '', '', 'Y', 'user', 'Y', 24, '');

-- ----------------------------
-- Table structure for pasangiklan
-- ----------------------------
DROP TABLE IF EXISTS `pasangiklan`;
CREATE TABLE `pasangiklan`  (
  `id_pasangiklan` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  PRIMARY KEY (`id_pasangiklan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pasangiklan
-- ----------------------------
INSERT INTO `pasangiklan` VALUES (23, 'Griyaparfum', 'admin', 'http://griya-parfum.co.cc', '345872griyaparfum_logo.jpg', '2011-06-22');
INSERT INTO `pasangiklan` VALUES (26, 'PT. PELANGI SAMUDERA INTERNATIONAL', 'admin', 'http://www.psimt.net', 'psim.jpg', '2011-09-29');
INSERT INTO `pasangiklan` VALUES (28, 'Penerbit Lokomedia', 'admin', 'http://bukulokomedia.com', '58898lokomedia.jpg', '2012-11-22');

-- ----------------------------
-- Table structure for playlist
-- ----------------------------
DROP TABLE IF EXISTS `playlist`;
CREATE TABLE `playlist`  (
  `id_playlist` int(5) NOT NULL AUTO_INCREMENT,
  `jdl_playlist` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `playlist_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gbr_playlist` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_playlist`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 58 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of playlist
-- ----------------------------
INSERT INTO `playlist` VALUES (50, 'Kajian Al-Hikmah', '', 'kajian-alhikmah', '146881kajian.jpg', 'Y');
INSERT INTO `playlist` VALUES (49, 'Tausiyah', '', 'tausiyah', '855407tausiyah.jpg', 'Y');
INSERT INTO `playlist` VALUES (51, 'Murottal', '', 'murottal', '222351murottal.jpg', 'Y');

-- ----------------------------
-- Table structure for poling
-- ----------------------------
DROP TABLE IF EXISTS `poling`;
CREATE TABLE `poling`  (
  `id_poling` int(5) NOT NULL AUTO_INCREMENT,
  `pilihan` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `status` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `rating` int(5) NOT NULL DEFAULT 0,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_poling`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 30 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of poling
-- ----------------------------
INSERT INTO `poling` VALUES (18, 'Siapakah Calon Gubernur dan Wakil Gubernur DKI Favorit Anda?', 'Pertanyaan', 'admin', 0, 'Y');
INSERT INTO `poling` VALUES (26, 'Joko Widodo-Basuki Tjahaja Purnama', 'Jawaban', 'admin', 9, 'Y');
INSERT INTO `poling` VALUES (25, 'Fauzi Bowo-Nachrowi Ramli', 'Jawaban', 'admin', 3, 'Y');

-- ----------------------------
-- Table structure for sekilasinfo
-- ----------------------------
DROP TABLE IF EXISTS `sekilasinfo`;
CREATE TABLE `sekilasinfo`  (
  `id_sekilas` int(5) NOT NULL AUTO_INCREMENT,
  `info` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  `gambar` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_sekilas`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sekilasinfo
-- ----------------------------
INSERT INTO `sekilasinfo` VALUES (1, 'Selamat menjalankan ibadah shaum ramadhan 1441H', '2020-04-25', '', 'Y');
INSERT INTO `sekilasinfo` VALUES (2, 'Penerimaan Siswa Baru PAUD Al-Hikmah mulai 3 Januari 2020 s/d 5 April 2020', '2020-04-25', '', 'Y');
INSERT INTO `sekilasinfo` VALUES (3, 'Perpisahan TK Al-Hikmah Angkatan 2019 dilaksanakan di Masjid CItra Raya Tanggal 20 Februari 2020', '2020-04-25', '', 'Y');

-- ----------------------------
-- Table structure for statistik
-- ----------------------------
DROP TABLE IF EXISTS `statistik`;
CREATE TABLE `statistik`  (
  `ip` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `tanggal` date NOT NULL,
  `hits` int(10) NOT NULL DEFAULT 1,
  `online` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of statistik
-- ----------------------------
INSERT INTO `statistik` VALUES ('127.0.0.2', '2009-09-11', 1, '1252681630');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-22', 1, '1358865974');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-23', 14, '1358913313');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-24', 34, '1359046647');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-25', 21, '1359051663');
INSERT INTO `statistik` VALUES ('::1', '2015-12-11', 1, '1449845354');
INSERT INTO `statistik` VALUES ('::1', '2015-12-12', 16, '1449938849');
INSERT INTO `statistik` VALUES ('::1', '2015-12-14', 2, '1450067241');
INSERT INTO `statistik` VALUES ('::1', '2015-12-20', 3, '1450605849');
INSERT INTO `statistik` VALUES ('::1', '2016-01-19', 2, '1453191174');
INSERT INTO `statistik` VALUES ('::1', '2020-04-16', 5, '1587053769');
INSERT INTO `statistik` VALUES ('::1', '2020-04-20', 57, '1587346423');
INSERT INTO `statistik` VALUES ('::1', '2020-04-24', 105, '1587718706');
INSERT INTO `statistik` VALUES ('::1', '2020-04-25', 212, '1587778872');

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag`  (
  `id_tag` int(5) NOT NULL AUTO_INCREMENT,
  `nama_tag` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tag_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `count` int(5) NOT NULL,
  PRIMARY KEY (`id_tag`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 49 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES (30, 'Pendidikan', '', 'pendidikan', 7);
INSERT INTO `tag` VALUES (29, 'Ekonomi', '', 'ekonomi', 7);
INSERT INTO `tag` VALUES (22, 'Hiburan', '', 'hiburan', 16);
INSERT INTO `tag` VALUES (28, 'Teknologi', '', 'teknologi', 8);
INSERT INTO `tag` VALUES (27, 'Metropolitan', '', 'metropolitan', 29);
INSERT INTO `tag` VALUES (26, 'Nasional', '', 'nasional', 38);
INSERT INTO `tag` VALUES (25, 'Kesehatan', '', 'kesehatan', 14);
INSERT INTO `tag` VALUES (24, 'Olahraga', '', 'olahraga', 10);
INSERT INTO `tag` VALUES (23, 'Dunia Islam', '', 'dunia-islam', 40);
INSERT INTO `tag` VALUES (21, 'Kuliner', '', 'kuliner', 17);
INSERT INTO `tag` VALUES (20, 'Komunitas', '', 'komunitas', 2);
INSERT INTO `tag` VALUES (31, 'Politik', '', 'politik', 15);
INSERT INTO `tag` VALUES (32, 'Seni & Budaya', '', 'seni--budaya', 4);
INSERT INTO `tag` VALUES (33, 'Sekitar Kita', '', 'sekitar-kita', 9);
INSERT INTO `tag` VALUES (34, 'Wisata', '', 'wisata', 4);
INSERT INTO `tag` VALUES (35, 'Gaya Hidup', '', 'gaya-hidup', 4);
INSERT INTO `tag` VALUES (36, 'Hukum', '', 'hukum', 13);
INSERT INTO `tag` VALUES (37, 'Film', '', 'film', 24);
INSERT INTO `tag` VALUES (38, 'Musik', '', 'musik', 11);
INSERT INTO `tag` VALUES (39, 'Daerah', '', 'daerah', 23);
INSERT INTO `tag` VALUES (40, 'Internasional', '', 'internasional', 22);
INSERT INTO `tag` VALUES (41, 'Bola', '', 'bola', 19);
INSERT INTO `tag` VALUES (42, 'Televisi', '', 'televisi', 2);
INSERT INTO `tag` VALUES (43, 'Selebritis', '', 'selebritis', 5);
INSERT INTO `tag` VALUES (44, 'Tragedi Tugu Tani', '', 'tragedi-tugu-tani', 3);
INSERT INTO `tag` VALUES (45, 'Pilkada DKI', '', 'pilkada-dki', 0);
INSERT INTO `tag` VALUES (46, 'Tokoh', '', 'tokoh', 0);
INSERT INTO `tag` VALUES (47, 'Piala Eropa', '', 'piala-eropa', 23);
INSERT INTO `tag` VALUES (48, 'Sejarah Indonesia', 'admin', 'sejarah-indonesia', 18);

-- ----------------------------
-- Table structure for tagvid
-- ----------------------------
DROP TABLE IF EXISTS `tagvid`;
CREATE TABLE `tagvid`  (
  `id_tag` int(5) NOT NULL AUTO_INCREMENT,
  `nama_tag` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tag_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `count` int(5) NOT NULL,
  PRIMARY KEY (`id_tag`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 34 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for templates
-- ----------------------------
DROP TABLE IF EXISTS `templates`;
CREATE TABLE `templates`  (
  `id_templates` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `pembuat` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `folder` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_templates`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of templates
-- ----------------------------
INSERT INTO `templates` VALUES (12, 'Biru', 'admin', 'Rizal Faizal', 'layout/biru', 'N');
INSERT INTO `templates` VALUES (13, 'Merah', 'admin', 'Rizal Faizal', 'layout/merah', 'Y');
INSERT INTO `templates` VALUES (16, 'Hijau', 'admin', 'Rizal Faizal', 'layout/hijau', 'N');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `blokir` enum('Y','N') CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', 'ustadalbahra@gmail.com', '', 'admin.png', 'admin', 'N', 'q173s8hs1jl04st35169ccl8o7');

-- ----------------------------
-- Table structure for users_modul
-- ----------------------------
DROP TABLE IF EXISTS `users_modul`;
CREATE TABLE `users_modul`  (
  `id_umod` int(11) NOT NULL AUTO_INCREMENT,
  `id_session` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_modul` int(11) NOT NULL,
  PRIMARY KEY (`id_umod`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 107 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users_modul
-- ----------------------------
INSERT INTO `users_modul` VALUES (88, '5bi6b98a7r02hvh15dsog2vfo2', 44);
INSERT INTO `users_modul` VALUES (87, '5bi6b98a7r02hvh15dsog2vfo2', 43);
INSERT INTO `users_modul` VALUES (102, '5bi6b98a7r02hvh15dsog2vfo2', 34);
INSERT INTO `users_modul` VALUES (80, '5bi6b98a7r02hvh15dsog2vfo2', 18);
INSERT INTO `users_modul` VALUES (101, '5bi6b98a7r02hvh15dsog2vfo2', 2);

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `id_video` int(5) NOT NULL AUTO_INCREMENT,
  `id_playlist` int(5) NOT NULL,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `jdl_video` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `video_seo` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `gbr_video` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `video` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `youtube` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dilihat` int(7) NOT NULL DEFAULT 1,
  `hari` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `tagvid` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_video`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 175 CHARACTER SET = latin1 COLLATE = latin1_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES (169, 51, 'admin', 'Murottal Merdu Menyentuh Hati Salim Bahanan Yasin,Ar rahman,Al kahfi,Al waqiah,Al mulk', 'murottal-merdu-menyentuh-hati-salim-bahanan-yasinar-rahmanal-kahfial-waqiahal-mulk', 'Murottal by : Salim Bahanan\r\nSource         : Ammar TV\r\n', '', '', 'https://www.youtube.com/embed/eEy2Ba-H5yE', 1, 'Jumat', '2020-04-24', '14:46:58', '');
INSERT INTO `video` VALUES (170, 51, 'admin', 'Surah Al Kahfi Merdu dan Terjemah - Ismail Annuri', 'surah-al-kahfi-merdu-dan-terjemah--ismail-annuri', '<span class=\"style-scope yt-formatted-string\">Bacaan Quran Merdu Surah Al Kahfi dan Terjemah Qari Ismail Annuri</span>\r\n', '', '', 'https://www.youtube.com/embed/9HedHbIlYUo', 1, 'Jumat', '2020-04-24', '14:48:20', '');
INSERT INTO `video` VALUES (171, 51, 'admin', 'MUROTAL QURAN (SURAT ALI IMRON 189-194)', 'murotal-quran-surat-ali-imron-189194', '<span class=\"style-scope yt-formatted-string\">Smoga berkah membaca dan mendengar al quran menjadi amal kebaikan untuk kita semua \r\nAamiiinðŸ˜‡</span>\r\n', '', '', 'https://www.youtube.com/embed/tp7HIGDEoNI', 1, 'Jumat', '2020-04-24', '14:50:22', '');
INSERT INTO `video` VALUES (172, 51, 'admin', 'Murottal Syaikh Mansur Al- Salimi Hafidzahullah', 'murottal-syaikh-mansur-al-salimi-hafidzahullah', 'Murottal Syaikh Mansur Al- Salimi Hafidzahullah\r\n', '', '', 'https://www.youtube.com/embed/oeOJqcb5Gho', 1, 'Jumat', '2020-04-24', '14:51:13', '');
INSERT INTO `video` VALUES (173, 51, 'admin', 'Surah An Najm - Salah Musally Terjemah Indonesia', 'surah-an-najm--salah-musally-terjemah-indonesia', 'An Najm\r\nSurah An Najm ayat 1-32\r\nQori : Salah Musally\r\n', '', '', 'https://www.youtube.com/embed/as2pfdbGugQ', 1, 'Jumat', '2020-04-24', '14:51:56', '');
INSERT INTO `video` VALUES (174, 51, 'admin', 'ADEM, IRAMA NAHAWAND DI AYAT PANJANG', 'adem-irama-nahawand-di-ayat-panjang', '<span class=\"style-scope yt-formatted-string\">Surah Ali &#39;Imran Ayat 1 s/d 9\r\nIrama Nahawand\r\nUst. Bilal Attaki</span>\r\n', '', '', 'https://www.youtube.com/embed/ZcacnC9kjXE', 1, 'Jumat', '2020-04-24', '14:52:39', '');
INSERT INTO `video` VALUES (166, 49, 'admin', 'Motivasi Kehidupan dari Ustadz Adi Hidayat LC MA', 'motivasi-kehidupan-dari-ustadz-adi-hidayat-lc-ma', '<span class=\"style-scope yt-formatted-string\">Hidup di dunia memang unik. Sebagian orang sibuk dengan urusan dunianya, sementara sebagian lainnya sibuk mempersiapkan kehidupan akhirat.\r\nSimak Motivasi Kehidupan dari Ustadz Adi Hidayat LC MA. Asli bikin Terharu !!</span>\r\n', '', '', 'https://www.youtube.com/embed/LNRtmokXy5c', 1, 'Jumat', '2020-04-24', '14:43:26', '');
INSERT INTO `video` VALUES (167, 49, 'admin', 'Tausiyah Ust. Evie Effendi, Nikmat Tuhan Mana Lagi Yang Engkau Dustakan', 'tausiyah-ust-evie-effendi-nikmat-tuhan-mana-lagi-yang-engkau-dustakan', 'Tausiyah Ust. Evie Effendi, &quot;Nikmat Tuhan Mana Lagi Yang Engkau Dustakan\r\n', '', '', 'https://www.youtube.com/embed/xpL2W4r5Iwk', 1, 'Jumat', '2020-04-24', '14:44:18', '');
INSERT INTO `video` VALUES (168, 49, 'admin', 'Nasehat Untukmu Yang Jatuh Bangun Dalam Bertaubat', 'nasehat-untukmu-yang-jatuh-bangun-dalam-bertaubat', '<span class=\"style-scope yt-formatted-string\">ustadz, bagaimana kita melatih jiwa ini agar senantiasa sadar dengan pengawasan Allah, karena seseorang sering melakukan kemaksiatan dalam kesendirian kemudian taubat, kemudian maksiat lagi, kemudian taubat, kemudian terjatuh lagi. Mohon diberikan nasihat antum ustadz</span>\r\n', '', '', 'https://www.youtube.com/embed/3aUfzCYi_xw', 1, 'Jumat', '2020-04-24', '14:45:08', '');
INSERT INTO `video` VALUES (161, 50, 'admin', 'Ketika Memilih Jalan Allah | Kajian Ust. Hanan Attaki, Lc.', 'ketika-memilih-jalan-allah--kajian-ust-hanan-attaki-lc', '<span class=\"style-scope yt-formatted-string\">Ketika Memilih Jalan Allah</span>\r\n', '', '', 'https://www.youtube.com/embed/ykF7DuWmIKg', 1, 'Jumat', '2020-04-24', '14:36:18', '');
INSERT INTO `video` VALUES (162, 0, 'admin', 'MENGHARUKAN... TAUSIYAH SYEKH ALI JABER TERKAIT PANDEMI COVID1', 'mengharukan-tausiyah-syekh-ali-jaber-terkait-pandemi-covid1', '<span class=\"style-scope yt-formatted-string\">Mari Bergabung Bersama Kami Mewujudkan 1 Juta Penghafal Al Quran Bersanad di Indonesia dengan ikut serta dalam Pembangunan Pesantren Syekh Ali Jaber.\r\n</span>\r\n', '', '', 'https://www.youtube.com/embed/fg8lD20ek4Q', 1, 'Jumat', '2020-04-24', '14:39:27', '');
INSERT INTO `video` VALUES (163, 0, 'admin', 'MENGHARUKAN... TAUSIYAH SYEKH ALI JABER TERKAIT PANDEMI COVID19', 'mengharukan-tausiyah-syekh-ali-jaber-terkait-pandemi-covid19', '<span class=\"style-scope yt-formatted-string\">Mari Bergabung Bersama Kami Mewujudkan 1 Juta Penghafal Al Quran Bersanad di Indonesia dengan ikut serta dalam Pembangunan Pesantren Syekh Ali Jaber.\r\n</span>\r\n', '', '', 'https://www.youtube.com/embed/fg8lD20ek4Q', 1, 'Jumat', '2020-04-24', '14:40:07', '');
INSERT INTO `video` VALUES (164, 49, 'admin', 'MENGHARUKAN... TAUSIYAH SYEKH ALI JABER TERKAIT PANDEMI COVID19', 'mengharukan-tausiyah-syekh-ali-jaber-terkait-pandemi-covid19', '<span class=\"style-scope yt-formatted-string\">Mari Bergabung Bersama Kami Mewujudkan 1 Juta Penghafal Al Quran Bersanad di Indonesia dengan ikut serta dalam Pembangunan Pesantren Syekh Ali Jaber.\r\n</span>\r\n', '', '', 'https://www.youtube.com/embed/fg8lD20ek4Q', 1, 'Jumat', '2020-04-24', '14:40:37', '');
INSERT INTO `video` VALUES (165, 49, 'admin', 'UAS Terbaru. Ramadhan Dan Idul Fitri 2020 Ibadah Dirumah Aja', 'uas-terbaru-ramadhan-dan-idul-fitri-2020-ibadah-dirumah-aja', '<span class=\"style-scope yt-formatted-string\">2020 Ibadah Ramdhan dan Idul Fitri Dirumah Aja. Jangan Katakan Kita Meninggalkan Masjid, Karena Ini  Sunnah Nabi Muhammad SAW Saat Dalam Situasi Wabah Penyakit Menular Seperti Sekarang Ini. Ustadz Abdul Somad\r\n</span>\r\n', '', '', 'https://www.youtube.com/embed/quwn5a6_26w', 1, 'Jumat', '2020-04-24', '14:42:44', '');
INSERT INTO `video` VALUES (158, 50, 'admin', 'TIPS HIDUP TENANG UNTUK ANAK MILENIAL', 'tips-hidup-tenang-untuk-anak-milenial', '<span class=\"style-scope yt-formatted-string\">Ustad Das&#39;ad Latif  - TIPS HIDUP TENANG UNTUK ANAK MILENIAL</span>\r\n', '', '', 'https://www.youtube.com/embed/g7dbLz2ItQ4', 1, 'Jumat', '2020-04-24', '14:32:06', '');
INSERT INTO `video` VALUES (159, 50, 'admin', 'Kajian Dzuhur - Konsep Bala dalam Menyikapi Wabah', 'kajian-dzuhur--konsep-bala-dalam-menyikapi-wabah', '<span class=\"style-scope yt-formatted-string\">Kajian Dzuhur Ramadhan 1441 H PHE Mengaji\r\nTema: Konsep Bala&#39; dalam Menyikapi Wabah serta Kebijakan Menghadapi Tha&#39;un pada Tahun 749 H\r\nPemateri: Ustadz Agung Waspodo</span>\r\n', '', '', 'https://www.youtube.com/embed/5YuyZmHkW9o', 1, 'Jumat', '2020-04-24', '14:34:43', '');
INSERT INTO `video` VALUES (160, 50, 'admin', 'Kajian Makrifatullah bersama KH. Abdullah Gymnastiar dan Wali Kota Bogor Bima Arya', 'kajian-makrifatullah-bersama-kh-abdullah-gymnastiar-dan-wali-kota-bogor-bima-arya', 'Kajian Ma&#39;rifatullah bersama KH. Abdullah Gymnastiar dan Bima Arya Sugiarto.\r\n', '', '', 'https://www.youtube.com/embed/afsWowEtNHc', 1, 'Jumat', '2020-04-24', '14:35:42', '');
INSERT INTO `video` VALUES (157, 50, 'admin', 'SETELAH IMSAK APAKAH MASIH BOLEH MAKAN DAN MINUM.', 'setelah-imsak-apakah-masih-boleh-makan-dan-minum', 'engan menyebarkan suatu kebaikan, insyaAllah sekecil apapun itu akan \r\nmenjadikan amal kebaikan dan bekal untuk kita nanti di akhirat.\r\nRasulullah Sallallahu &#39;alaihi wasallam bersabda : \r\nÙ…ÙŽÙ†Ù’ Ø¯ÙŽÙ„ÙŽÙ‘ Ø¹ÙŽÙ„ÙŽÙ‰ Ø®ÙŽÙŠÙ’Ø±Ù ÙÙŽÙ„ÙŽÙ‡Ù Ù…ÙØ«Ù’Ù„Ù Ø£ÙŽØ¬Ù’Ø±Ù ÙÙŽØ§Ø¹ÙÙ„ÙÙ‡Ù\r\n&ldquo;Siapa yang menunjukkan suatu kebaikan dia akan mendapatkan pahala \r\nseperti yang melakukannya.&rdquo;\r\nMari kita Mempererat Ukhuwah Islamiyah, Menjadi Ummat Terbaik dengan \r\nSaling Menasehati, Silahkan Share Video ini Kepada Sahabat, Keluarga dan\r\nOrang orang yang Kita Cintai karena Allah Ta&#39;ala. \r\nTerimakasih supportnya untuk channel KAJIAN ISLAM dan semoga channel ini\r\nbisa tetap istiqomah memberikan konten islami dan bisa menjadi amal \r\nbuat kita semua serta menjadi ilmu yang bermanfaat dunia akhirat. Semoga\r\nmedia dakwah ini bisa terus berkembang luas di berbagai media social \r\ndan disukai oleh semua kalangan\r\n', '', '', 'https://www.youtube.com/embed/rlgFXnX4OMY', 1, 'Jumat', '2020-04-24', '14:30:34', '');

SET FOREIGN_KEY_CHECKS = 1;
