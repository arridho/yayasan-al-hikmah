<ul class="breadcrumbs">
            <li class="home"><a href="index.html">Home</a></li>
            <li class="current">Hubungi Kami</li>
          </ul>
          <h2 class="page-title">Hubungi Kami</h2>
          <div id="post_content" class="post_content" role="main">
            <article class="page hentry">
              <div class="post_content">
                <div id="" class=""><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2169519457143!2d106.52047771415482!3d-6.235107995486607!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e42077ac886137f%3A0x302c58176365ad12!2sPerumahan%20Citra%20Raya%20Tangerang!5e0!3m2!1sen!2sid!4v1587776917053!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
                <br><br>
                <div class="sc_columns sc_columns_count_2">
                  <div class="content odd first">
                    <?php
                    $identitas=mysqli_query($koneksi,"SELECT * FROM identitas");
                    $i=mysqli_fetch_array($identitas);
                    ?>
                    <h4>Kunjungi Kami</h4>
                    <p>
                        Alamat  : <?php echo $i['alamat'] ?><br>
                        Telp    : <?php echo $i['no_telp'] ?><br>
                        email   : <?php echo $i['email'] ?><br>
                        website : <?php echo $i['url'] ?><br>
	     Instagram : @<?php echo $i['facebook'] ?><br>
                    </p>
                  </div>
                </div>

                <div class="sc_contact_form">
                  <h3 class="title">&nbsp;</h3>
                  <span class="description">Hubungi kami jika anda punya pertanyaan. Lengkapi form di bawah ini</span>
                  <form method="post" action="aksi-hubungi.php">
                    <div class="field">
                      <input type="text" id="" name="nama">
                      <label for="" class="required">Nama</label>
                    </div>
                    <div class="field">
                      <input type="text" id="" name="email">
                      <label for="" class="required">Email</label>
                    </div>
                    <div class="field">
                      <input type="text" id="" name="subjek">
                      <label for="">Subjek</label>
                    </div>
                    <div class="field">
                      <textarea id="" name="pesan"></textarea>
                    </div>
                     <input type="submit"  value="Kirim Pesan"> 
                  </form>
                </div>
              </div>
            </article>
          </div>