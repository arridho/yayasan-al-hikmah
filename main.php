<?php
//session_start();
// Panggil semua fungsi yang dibutuhkan (semuanya ada di folder config)
include "config/koneksi.php";
include "config/fungsi_indotgl.php";
//include "config/class_paging.php";
//include "config/fungsi_combobox.php";
include "config/library.php";
//include "config/fungsi_autolink.php";
//include "config/fungsi_badword.php";
//include "config/fungsi_kalender.php";
//include "config/option.php";
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Yayasan Al-Hikmah Citra Raya Tangerang</title>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="style/ie.css" type="text/css" media="all">
<![endif]-->
<link rel="stylesheet" href="style/style.css" type="text/css" media="all">
<link rel="stylesheet" href="style/responsive.css" type="text/css" media="all">
</head>
<body class="wide">
<!--[if lt IE 8]>
<div class="sc_infobox sc_infobox_style_error">
It looks like you're using an old version of Internet Explorer. For the best web site experience, please <a href="http://windows.microsoft.com/en-us/internet-explorer/ie-10-worldwide-languages">update your browser</a> or learn how to <a href="http://browsehappy.com">browse happy</a>!
</div>
<![endif]-->
<div id="page"> 
  
  <!-- header begin -->
  <header role="banner" class="site-header" id="header">
    <div>
      <section class="top">
        <div class="inner clearboth">
          <div class="top-left">
            <div id="jclock1" class="simpleclock"></div>
          </div>
          <div class="top-center">
            <div class="block_top_menu">
             
            </div>
          </div>
        </div>
      </section>
      <section class="section2">
        <div class="inner">
          <div class="section-wrap clearboth">
            <div class="block_social_top">
              <div class="icons-label">Follow us: </div>
	<ul>
                  <li><a class="tw" href="mailto:yalhikmahcitra@gmail.com" title="Twitter">Twitter</a></li>
                  <li><a class="fb" href="#" title="Facebook">Facebook</a></li>
                  <li><a class="rss" href="https://www.instagram.com/yalhikmahcitra/" title="RSS">RSS</a></li>
                  <li><a class="gplus" href="https://www.youtube.com/channel/UCZN5v1NgBrKuC1YZA9Hn7aA" title="Google+">Google+</a></li>
                  <li><a class="vim" href="#" title="Vimeo">Vimeo</a></li>
                  <li><a class="tmb" href="#" title="Tumblr">Tumblr</a></li>
                  <li><a class="pin" href="#" title="Pinterest">Pinterest</a></li>
                </ul>
            </div>
            <div class="newsletter">
              <div class="newsletter-popup"> <span class="bg"><span></span></span>
                <div class="indents">
                  <form target="_blank" method="post" action="">
                    <div class="field">
                      <input type="text" class="w_def_text" title="Enter Your Email Address" name="email" placeholder="Enter Your E-mail addres">
                    </div>
                    <input type="submit" value="Subscribe" class="button">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
	<div class="boxed2">
      <section class="section3">
        <div class="section-wrap clearboth">
          <div class="name-and-slogan">
            <h1 class="site-title"><a rel="home" title="PrimeTime" href="index.html"> <img alt="logo" src="images/logo.png"> </a></h1>
          </div>
        </div>
      </section>
      <div class="headStyleMenu">
        <section class="section-nav">
          <nav role="navigation" class="navigation-main">
            <ul class="clearboth mainHeaderMenu">
              <li class="home"><a href="index.html"></a></li>
              <li class="red"><a href="index.html">Home</a></li>            
              <li class="orange"><a href="tentang-kami.html">Tentang Kami</a></li>
              <li class="blue"><a href="">Pendidikan</a>
                <ul>
                  <li><a href="paud.html">PAUD</a></li>
                  <li><a href="tk.html">TK</a></li>
	<li><a href="tpa.html">TPA</a></li>
                </ul>
              </li>
              <li class="green"><a href="dakwah.html">Dakwah</a></li>
              <li class="sky-blue"><a href="sosial.html">Sosial</a></li>
              <li class="purple"><a href="tausiyah.html">Tausiayah Online</a></li>
              <li class="red"><a href="berita.html">Berita</a></li>
              <li class="purple"><a href="artikel.html">Artikel</a></li>
              <li class="orange"><a href="hubungi-kami.html">Hubungi Kami</a></li>
            </ul>
          </nav>
        </section>
        <!-- /#site-navigation --> 
        <!-- mobile menu -->
       <section class="section-navMobile">
          <div class="mobileMenuSelect"><span class="icon"></span>Home</div>
          <ul class="clearboth mobileHeaderMenuDrop">
            <li class="home"><a href="index.html"></a></li>
              <li class="red"><a href="index.html">Home</a></li>            
              <li class="orange"><a href="tentang-kami">Tentang Kami</a></li>
              <li class="blue"><a href="">Pendidikan</a>
                <ul>
                  <li><a href="paud.html">PAUD</a></li>
                  <li><a href="tk.html">TK</a></li>
	<li><a href="tpa.html">TPA</a></li>
                </ul>
              </li>
              <li class="green"><a href="dakwah.html">Dakwah</a></li>
              <li class="sky-blue"><a href="sosial.html">Sosial</a></li>
              <li class="purple"><a href="tausiyah.html">Tausiayah Online</a></li>
              <li class="orange"><a href="hubungi-kami.html">Hubungi Kami</a></li>
          </ul>
        </section>
        <!-- /mobile menu --> 
      </div>
	  
      <section class="news-ticker"> 
        <!-- Recent News slider -->
        <div id="flexslider-news" class="header_news_ticker">
          <ul class="news slides" >
            <?php
        	 $sekilas=mysqli_query($koneksi,"SELECT * FROM sekilasinfo ORDER BY id_sekilas DESC");
         	 while ($s=mysqli_fetch_array($sekilas)){
         	 echo "
          	    <li><a href=''>$s[info]</a></li>";
           	 }
          ?>
          </ul>
        </div>
        <!-- /Recent News slider --> 
      </section>
    </div>
  </header>
  </div><!-- / boxed2  -->
  <!-- / header  -->
 <div class="boxed2">
  <div class="right_sidebar" id="main">
    <div class="inner">
      <div class="general_content clearboth">
        <div class="main_content">
           <?php include "content.php";  ?>
        </div>
        <!--/.main_content--> 
        
        <!-- .main_sidebar -->
        <div role="complementary" class="main_sidebar right_sidebar" id="secondary">
          <aside class="widget widget_news_combine" id="news-combine-widget-2">
            <div class="widget_header">
              <div class="widget_subtitle"><a class="lnk_all_news" href="">&nbsp</a></div>
              <h3 class="widget_title">Konten Islami</h3>
            </div>
            <div class="widget_body">
              <div class="block_news_tabs" id="tabs">
                <div class="tabs">
                  <ul>
                    <li><a href="#tabs1"><span>Kajian Al-Hikmah</span></a></li>
                    <li><a href="#tabs2"><span>Tausiyah</span></a></li>
                    <li><a href="#tabs3"><span>Murottal</span></a></li>
                  </ul>
                </div>
                <!-- tab content goes here -->
                <div class="tab_content" id="tabs1">
                  	    <?php
	    $kajian=mysqli_query($koneksi,"SELECT * FROM video WHERE id_playlist='50' ORDER BY id_video DESC LIMIT 7");
	    $no=1;
	    while($ka=mysqli_fetch_array($kajian)){      
				  
	    echo "
	    <div class='block_home_news_post'>
	             <div class='info'>
		 <div class='date'>$ka[jam]</div>
	             </div>
                               <p class='title'><a href='video-$ka[id_video]-$ka[video_seo].html'>$ka[jdl_video].</a></p>
	     </div>";
	 }
	     ?>  	
                </div>
                <!-- tab content goes here -->
                <div class="tab_content" id="tabs2">
                   <?php
	    $kajian=mysqli_query($koneksi,"SELECT * FROM video WHERE id_playlist='49' ORDER BY id_video DESC LIMIT 7");
	    $no=1;
	    while($ka=mysqli_fetch_array($kajian)){      
				  
	    echo "
	    <div class='block_home_news_post'>
	             <div class='info'>
		 <div class='date'>$ka[jam]</div>
	             </div>
                               <p class='title'><a href='video-$ka[id_video]-$ka[video_seo].html'>$ka[jdl_video].</a></p>
	     </div>";
	 }
	     ?>  	
                </div>
                <!-- tab content goes here -->
                <div class="tab_content" id="tabs3">
                  <?php
	    $kajian=mysqli_query($koneksi,"SELECT * FROM video WHERE id_playlist='51' ORDER BY id_video DESC LIMIT 7");
	    $no=1;
	    while($ka=mysqli_fetch_array($kajian)){      
				  
	    echo "
	    <div class='block_home_news_post'>
	             <div class='info'>
		 <div class='date'>$ka[jam]</div>
	             </div>
                               <p class='title'><a href='video-$ka[id_video]-$ka[video_seo].html'>$ka[jdl_video].</a></p>
	     </div>";
	 }
	     ?>  	
                </div>
              </div>
            </div>
          </aside>
          <aside class="widget followers" id="followers-widget-2">
            <div class="block_subscribes_sidebar">
	<?php
	// Statistik user
	$ip      = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
	$tanggal = date("Ymd"); // Mendapatkan tanggal sekarang
	$waktu   = time(); // 
	
	
	
	// Mencek berdasarkan IPnya, apakah user sudah pernah mengakses hari ini 
	$s = mysqli_query($koneksi,"SELECT * FROM statistik WHERE ip='$ip' AND tanggal='$tanggal'");
	// Kalau belum ada, simpan data user tersebut ke database
	 if(mysqli_num_rows($s) == 0){
		mysqli_query($koneksi,"INSERT INTO statistik(ip, tanggal, hits, online) VALUES('$ip','$tanggal','1','$waktu')");
	} 
	else{
		mysqli_query($koneksi,"UPDATE statistik SET hits=hits+1, online='$waktu' WHERE ip='$ip' AND tanggal='$tanggal'");
	}

	$pengunjung       = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM statistik WHERE tanggal='$tanggal' GROUP BY ip"));
	//$totalpengunjung  = mysqli_result(mysqli_query($koneksi,"SELECT COUNT(hits) FROM statistik"), 0); 
	//$hits             = mysqli_fetch_assoc(mysqli_query($koneksi,"SELECT SUM(hits) as hitstoday FROM statistik WHERE tanggal='$tanggal' GROUP BY tanggal")); 
	//$totalhits        = mysqli_result(mysqli_query($koneksi,"SELECT SUM(hits) FROM statistik"), 0); 
	//$res      = mysqli_query($koneksi,"SELECT SUM(hits) FROM statistik"); 
	//$row = mysqli_fetch_array($res['hits']);
	//$tothitsgbr       = mysqli_result(mysqli_query($koneksi,"SELECT SUM(hits) FROM statistik"), 0); 
	//$bataswaktu       = time() - 300;
	$pengunjungonline = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM statistik WHERE online > '$bataswaktu'"));
	
	if (!function_exists('mysqli_result')) {
	 function mysqli_result($res, $row, $field=0) {
 	   $res->data_seek($row);
	    $datarow = $res->fetch_array();
	    return $datarow[$field];
	  }
	}
			
	?>
              <div class="service facebook_info"> <a class="fb" href="http://www.facebook.com/envato"> <span class="num"><?php echo "$pengunjungonline"; ?></span> <span class="people">Total Pengunjung</span> </a> </div>
              <div class="service twitter_info"> <a class="tw" href="http://www.twitter.com/envato"> <span class="num"><?php echo "$pengunjung"; ?></span> <span class="people">User Online</span> </a> </div>
              <!--<div class="service"> <a class="rss" href="http://feeds.feedburner.com/envato"> <span class="num"><?php echo "$tothitsgbr "; ?></span> <span class="people">Subs.</span> </a> </div>-->
            </div>
          </aside>
          
          <aside class="widget widget_recent_blogposts">
            <div class="widget_header">
              <div class="widget_subtitle">&nbsp;</div>
              <h3 class="widget_title">Agenda Kegiatan</h3>
            </div>
            <div class="widget_body">
              <ul class="slides">
                <li>

                  <?php
	 $agenda=mysqli_query($koneksi,"SELECT * FROM agenda ORDER BY rand() DESC LIMIT 5");
	 while($a=mysqli_fetch_array($agenda)){
	 $tgl_mulai = tgl_indo($a['tgl_mulai']);
	 $tgl_selesai = tgl_indo($a['tgl_selesai']);
	 $isi_agenda = strip_tags($a['isi_agenda']);
	 $isi = substr($isi_agenda,0,150);
	 $isi = substr($isi_agenda,0,strrpos($isi," ")); 
				  
	 echo "							  
                  <div class='article'>
                    <div class='pic'> <a class='w_hover img-link img-wrap' href='post-standart.html'><img  alt=''  src='foto_agenda/$a[gambar]'> </a> </div>
                    <div class='text'>
                      <p class='title'><a href='post-standart.html'>$a[tema].</a></p>
                      <div class='icons'>
                        <ul>
                          <li><a class='views' href='post-standart.html'> $tgl_mulai</a></li><br>
                          <li><a class='post_date' href='post-standart.html'>$a[jam]</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>";
	  }
	  ?>

                </li>
              </ul>
              <div class="pages_info"> <span class="cur_page">1</span> of <span class="all_pages">1</span> </div>
            </div>
          </aside>
          <!-- Recent posts -->
          <aside class="widget widget_recent_photos">
            <div class="widget_header">
              <div class="widget_subtitle"><a class="lnk_all_posts" href="media-photos.html">All photos</a></div>
              <h3 class="widget_title">Foto Kegiatan</h3>
            </div>
            <div class="widget_body">
              <div id="recent_photos_thumbs">
                <div class="flex-viewport" >
                  <ul class="slides">

                   <?php
	$album= mysqli_query($koneksi,"SELECT * FROM gallery order by id_gallery DESC LIMIT 6");
	 while($w=mysqli_fetch_array($album)){
                   echo "<li><img alt='' src='img_galeri/kecil_$w[gbr_gallery]' style='width:43px;height:44px'></li>";
	}
	 ?>
                  </ul>
                </div>
                <ul class="flex-direction-nav">
                  <li><a href="#" class="flex-prev flex-disabled"><span></span></a></li>
                  <li><a href="#" class="flex-next"><span></span></a></li>
                </ul>
              </div>
              <!--Recent photos-->
              <div id="recent_photos_slider" >
                <div class="flex-viewport" >
                  <ul class="slides" >
                    <?php
	$album= mysqli_query($koneksi,"SELECT * FROM gallery order by id_gallery DESC LIMIT 6");
	while($w=mysqli_fetch_array($album)){
	$jdl_album=($w['jdl_gallery']);
	echo "
	<li><a class='gal_link w_hover img-link prettyPhoto' href='img_galeri/$w[gbr_gallery]'><img alt='' src='img_galeri/$w[gbr_gallery]' style='width:276px;height:146px'> <span class='link-icon'></span> </a>
	         <div class='title'><a href='#'>$w[jdl_gallery]</a></div>
	</li>";
                    }
	?>
                  </ul>
                </div>
              </div>
            </div>
          </aside>
                   
          <!--Latest reviews-->
          <aside class="widget widget_recent_reviews" >
            <div class="widget_header">
              <div class="widget_subtitle"><a class="lnk_all_posts" href="reviews-all.html">All reviews</a></div>
              <h3 class="widget_title">Tausiyah Online</h3>
            </div>
            <div class="widget_body">
              <div class="recent_reviews">
                <ul class="slides">
                  <?php
                  $download=mysqli_query($koneksi,"SELECT * FROM download ORDER BY id_download LIMIT 4");
                  while ($d=mysqli_fetch_array($download))
                  {
                    echo  "
                  <li>
                    <div class='img_wrap'><a class='w_hover img_wrap' href='reviews-item-page.html'><img alt='' src='images/doc.jpg'></a></div>
                    <div class='extra_wrap'>
                      <div class='review-title'><a href='download.php?file=$d[nama_file]'>$d[judul]</a></div>
                      <div class='post-info'>
                       $d[hits] kali di download</div>
                    </div>
                  </li>";
                  }
                  ?>
                </ul>
              </div>
            </div>
          </aside>
         
        <aside class="widget widget_latest_comments" >
           <div class="widget_header">
              <h3 class="widget_title">Brosur</h3>
            </div>      
            <div class="widget_body">
	<?php
                  $pasangiklan=mysqli_query($koneksi,"SELECT * FROM pasangiklan ORDER BY id_pasangiklan LIMIT 3");
                  while ($iklan=mysqli_fetch_array($pasangiklan))
                  {
		echo "<a href='foto_pasangiklan/$iklan[gambar]'><img src='foto_pasangiklan/$iklan[gambar]'></a>";
             	}
	?>
            </div>
          </aside>    

        </div>
        <!-- /.main_sidebar --> 
        
      </div>
      <!-- /.general_content --> 
    </div>
    <!-- /.inner --> 
  </div>
  <!-- /#main -->
 </div> <!-- /#boxed2 -->
  <footer role="contentinfo" class="site-footer" id="footer">
    <section class="ft_section_2">
      <div class="footer-wrapper">
        <ul id="footer_menu">
          <li><a href="index.html">Home</a></li>
          <li><a href="hubungi-kami.html">Contact Us</a></li>
        </ul>
        <div class="copyright">
          <div class="footer_text">&copy; 2020. Yayasan Al-Hikmah Citra Raya</div>
        </div>
      </div>
    </section>
  </footer>
  
  <!-- PopUp -->
  <div id="overlay"></div>
  <!-- Login form -->
  <div class="login-popup popUpBlock" >
    <div class="popup"> <a class="close" href="#">×</a>
      <div class="content">
        <div class="title">Authorization</div>
        <div class="form">
          <form name="login_form" method="post">
            <div class="col1">
              <label for="log">Login</label>
              <div class="field">
                <input type="text" id="log" name="log">
              </div>
            </div>
            <div class="col2">
              <label for="pwd">Password</label>
              <div class="field">
                <input type="password" id="pwd" name="pwd">
              </div>
            </div>
            <div class="extra-col">
              <ul>
                <li><a class="register-redirect" href="#">Registration</a></li>
              </ul>
            </div>
            <div class="column button">
              <input type="hidden" value="" name="redirect_to">
              <a class="enter" href="#"><span>Login</span></a>
              <div class="remember">
                <input type="checkbox" value="forever" id="rememberme" name="rememberme">
                <label for="rememberme">Remember me</label>
              </div>
            </div>
            <div class="soc-login">
              <div class="section-title">Enter with social networking</div>
              <div class="section-subtitle">Unde omnis iste natus error sit voluptatem.</div>
              <ul class="soc-login-links">
                <li class="tw"><a href="#"><em></em><span>With Twitter</span></a></li>
                <li class="fb"><a href="#"><em></em><span>Connect</span></a></li>
                <li class="gp"><a href="#"><em></em><span>With Google +</span></a></li>
              </ul>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /Login form --> 
  
  <!-- Registration form -->
  <div class="registration-popup popUpBlock">
    <div class="popup"> <a class="close" href="#">×</a>
      <div class="content">
        <div class="title">Registration</div>
        <div class="form">
          <form name="registration_form" method="post">
            <div class="col1">
              <div class="field">
                <div class="label-wrap">
                  <label class="required" for="registration_form_username">Name</label>
                </div>
                <div class="input-wrap">
                  <input type="text" id="registration_form_username" name="registration_form_username">
                </div>
              </div>
            </div>
            <div class="col2">
              <div class="field">
                <div class="label-wrap">
                  <label class="required" for="registration_form_email">Email</label>
                </div>
                <div class="input-wrap">
                  <input type="text" id="registration_form_email" name="registration_form_email">
                </div>
              </div>
            </div>
            <div class="col1">
              <div class="field">
                <div class="label-wrap">
                  <label class="required" for="registration_form_pwd1">Password</label>
                </div>
                <div class="input-wrap">
                  <input type="password" id="registration_form_pwd1" name="registration_form_pwd1">
                </div>
              </div>
            </div>
            <div class="col2">
              <div class="field">
                <div class="label-wrap">
                  <label class="required" for="registration_form_pwd2">Confirm Password</label>
                </div>
                <div class="input-wrap">
                  <input type="password" id="registration_form_pwd2" name="registration_form_pwd2">
                </div>
              </div>
            </div>
            <div class="extra-col">
              <ul>
                <li><a class="autorization-redirect" href="#">Autorization</a></li>
              </ul>
            </div>
            <div class="column button"> <a class="enter" href="#"><span>Register</span></a>
              <div class="notice">* All fields required</div>
            </div>
            <div class="result sc_infobox"></div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /Registration form --> 
  
  <!-- go Top--> 
  <a id="toTop" href="#"><span></span></a> </div>
<!--page--> 

<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script> 
<script type="text/javascript" src="js/superfish.js"></script> 
<script type="text/javascript" src="js/jquery.jclock.js"></script> 
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script> 
<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script> 
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script> 
<script type="text/javascript" src="js/jquery.elastislide.js"></script> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
<script type="text/javascript" src="js/googlemap_init.js"></script> 
<script type="text/javascript" src="js/mediaelement.min.js"></script> 
<script type="text/javascript" src="js/lib.js"></script>
</body>

<!-- Mirrored from primehtml.themerex.net/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Dec 2015 08:20:22 GMT -->
</html> 