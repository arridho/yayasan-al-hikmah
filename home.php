<div class="content-area" id="primary">
            <div role="main" class="site-content" id="content"> 
              
              <!--slider1-->
              <div class="block_home_slider style1">
                <div class="slider-wrapper">
                  <div class="flexslider" id="home_slider1">
                    <ul class="slides">
	<?php

	$terkini=mysqli_query($koneksi,"SELECT * FROM berita WHERE headline='Y' ORDER BY id_berita DESC LIMIT 4");
	$no=1;
	while($t=mysqli_fetch_array($terkini)){      
									
	$isi_berita = strip_tags($t['isi_berita']); 
	$isi = substr($isi_berita,0,150); 
	$isi = substr($isi_berita,0,strrpos($isi," "));
	echo "
                      <li>
                        <div class='slide'><a href='post-standart.html'><img alt='$t[judul]' src='foto_berita/$t[gambar]' style='width:664px;height:361px'></a>
                          <div class='caption'>
                            <p class='title'>$t[judul]</p>
                            <p class='body'>$isi...</p>
                          </div>
                        </div>
                      </li>";
	}
	?>

                    </ul>
                  </div>
                  <ul id="paging_controls">
	<?php
	$terkinia=mysqli_query($koneksi,"SELECT * FROM berita WHERE headline='Y' ORDER BY id_berita DESC LIMIT 4");
	while($t1=mysqli_fetch_array($terkinia)){      
	$tgl=tgl_indo($t1['tanggal']);
	echo "
                    <li>
                      <div class='inner'>
                        <div class='slide-title'>$t1[judul].</div>
                        <div class='post-date'>$tgl</div>
                      </div>
                    </li>
	 </li>";
	}
	?>
                  
                  </ul>
                </div>
              </div>
              <!-- Recent News -->
              <div class="home_category_news clearboth">
                <div class="border-top"></div>
                <h2 class="block-title">Sambutan-Sambutan</h2>
                <div class="items-wrap">
	
	<?php
	$sambutan=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='53' ORDER BY id_berita+1 DESC LIMIT 2");
	$no=1;
	while($sam=mysqli_fetch_array($sambutan)){      
					
	 $isi_berita = strip_tags($sam['isi_berita']); 
	 $isi = substr($isi_berita,0,270); 
	 $isi = substr($isi_berita,0,strrpos($isi," "));
					
	$tgl=tgl_indo($sam['tanggal']);

	echo "
                  <div class='block_home_post first-post'>
                    <div class='post-image'><a class='img-link img-wrap w_hover' href='sambutan-$sam[id_berita]-sam[judul_seo].html'> <img  alt='$sam[judul]'  src='foto_berita/$sam[gambar]'> <span class='link-icon'></span> </a> </div>
                    <div class='post-content'>
                      <div class='title'><a href='sambutan-$sam[id_berita]-sam[judul_seo].html'>$sam[judul]</a></div>
                    </div>
                    <div class='post-info'>
                      <div class='post_date'>$tgl</div>
                      </div>
                    <div class='post-body'>$isi...Selengkapnya</div>
                  </div>";
	}
	
					 
	$profil=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='36' ORDER BY id_berita+1 ASC LIMIT 4");
	$no=1;
	while($p=mysqli_fetch_array($profil)){      
	$tgl=tgl_indo($p['tanggal']);
                  echo "
	<div class='block_home_post bd-bot'>
                    <div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$p[judul]'  src='foto_berita/$p[gambar]' style='width:85px;height:63px'> <span class='link-icon'></span> </a> </div>
                    <div class='post-content'>
                      <div class='title'><a href='detail-$p[id_berita]-$p[judul_seo].html'>$p[judul].</a></div>
                    </div>
                    <div class='post-info'>
                      <div class='post_date'>$tgl</div>
                    </div>
                  </div>";
	}
	 ?>
              
                </div>
              </div>
              <!-- /Recent News --> 
              
          
              <div class="two_columns_news clearboth"> 
                
                <!-- Recent News -->
                <div class="home_category_news_small clearboth">
                  <div class="border-top"></div>
                  <h2 class="block-title">TK AL-Hikmah</h2>
                  <div class="items-wrap">
	<?php				 
	$sql_tk=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='57' ORDER BY id_berita+1 DESC LIMIT 1");
	$no=1;
	while($tk=mysqli_fetch_array($sql_tk)){      
					
	$isi_berita = strip_tags($tk['isi_berita']); 
	$isi = substr($isi_berita,0,170); 
	$isi = substr($isi_berita,0,strrpos($isi," "));
					
	$tgl=tgl_indo($tk['tanggal']);
	echo "
	 <div class='block_home_post first-post'>
	     <div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$tk[judul]'  src='foto_berita/$tk[gambar]' style='width:300px;height:176px'> <span class='link-icon'></span> </a> </div>
		<div class='post-content'>
		     <div class='title'><a href='detail-$tk[id_berita]-$tk[judul_seo].html'>$tk[judul]</a></div>
		</div>
		 <div class='post-info'>
		      <div class='post_date'>$tgl</div>
		 </div>
		 <div class='post-body'>$isi...</div>
	 </div>";
	}
					
                   $sql_tk_=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='57' ORDER BY id_berita+1 ASC LIMIT 2");
	$no=1;
	while($tk_=mysqli_fetch_array($sql_tk_)){      
	$tgl=tgl_indo($tk_['tanggal']);
                  echo "
	 <div class='block_home_post bd-bot'>
                    <div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$tk_[judul]'  src='foto_berita/$tk_[gambar]' style='width:85px;height:63px'> <span class='link-icon'></span> </a> </div>
                    <div class='post-content'>
                      <div class='title'><a href='detail-$tk_[id_berita]-$tk_[judul_seo].html'>$tk_[judul].</a></div>
                    </div>
                    <div class='post-info'>
                      <div class='post_date'>$tgl</div>
                    </div>
                  </div>";
	  }
	  ?>
					
                  </div>
                  <div class="view-all"><a href="tk.html">View all</a></div>
                </div>
                <!-- /Recent News --> 
                
                <!-- Recent News -->
                <div class="home_category_news_small clearboth">
                  <div class="border-top"></div>
                  <h2 class="block-title">PAUD Al-Hikmah</h2>
                  <div class="items-wrap">
	<?php				 
	$sql_paud=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='56' ORDER BY id_berita DESC LIMIT 1");
	$no=1;
	while($paud=mysqli_fetch_array($sql_paud)){      
					
	$isi_berita = strip_tags($paud['isi_berita']); 
	$isi = substr($isi_berita,0,150); 
	$isi = substr($isi_berita,0,strrpos($isi," "));
					
	$tgl=tgl_indo($paud['tanggal']);
	echo "
	<div class='block_home_post first-post'>
	<div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$paud[judul]'  src='foto_berita/$paud[gambar]' style='width:300px;height:176px'> <span class='link-icon'></span> </a> </div>
		<div class='post-content'>
		       <div class='title'><a href='detail-$paud[id_berita]-$paud[judul_seo].html'>$paud[judul]</a></div>
		</div>
		<div class='post-info'>
		     <div class='post_date'>$tgl</div>
		</div>
		<div class='post-body'>$isi...</div>
	 </div>";
	}
					
                   $sql_paud_=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='56' ORDER BY id_berita+1 ASC LIMIT 2");
	$no=1;
	while($paud_=mysqli_fetch_array($sql_paud_)){      
									
					
					
	$tgl=tgl_indo($paud_['tanggal']);
                  echo "
	<div class='block_home_post bd-bot'>
                    <div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$paud_[judul]'  src='foto_berita/$paud_[gambar]' style='width:85px;height:63px'> <span class='link-icon'></span> </a> </div>
                    <div class='post-content'>
                      <div class='title'><a href='detail-$paud_[id_berita]-$paud_[judul_seo].html'>$paud_[judul].</a></div>
                    </div>
                    <div class='post-info'>
                      <div class='post_date'>$tgl</div>
                    </div>
                  </div>";
	}
	 ?>			
                  </div>
                  <div class="view-all"><a href="paud.html">View all </a></div>
                </div>
                <!-- /Recent News --> 
              </div>
              
              <!-- Homepage gallery -->
			  
              <section id="blog_posts" class="home_category_news clearboth">
			  <div class="border-top"></div>
              <h2 class="block-title">Artikel Islam</h2>
              <?php				 
	$artikel=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='47' ORDER BY id_berita DESC LIMIT 3");
	$no=1;
	while($art=mysqli_fetch_array($artikel)){      
					
	 $isi_berita = strip_tags($art['isi_berita']); 
	 $isi = substr($isi_berita,0,150); 
	 $isi = substr($isi_berita,0,strrpos($isi," "));
					
	 $tgl=tgl_indo($art['tanggal']);

	echo "
	<article>
	 <div class='pic'><a href='detail-$art[id_berita]-$art[judul_seo].html' class='w_hover img-link img-wrap'><img src='foto_berita/$art[gambar]' alt='' /> <span class='link-icon'></span> </a> </div>
	 <h3><a href='detail-$art[id_berita]-$art[judul_seo].html'>$art[judul]</a></h3>
	 <div class='post-info'><a href='' class='post_submitted'>Posted by $art[username]</a><a href='#' class='post_date'>$tgl</a></div>
	 <div class='text'> $isi...</div>
	 <a href='detail-$art[id_berita]-$art[judul_seo].html' class='more-link'>Selengkapnya<span></span></a> 
	</article>";
	}
	?>
				
	 <!-- /Homepage Gallery --> 
				  
	</section>
              
            
            </div>
            <!-- /#content --> 
          </div>
          <!-- /#primary --> 