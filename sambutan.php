<?php
$detail=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_berita='$_GET[id]'");
$d=mysqli_fetch_array($detail);
?>
<ul class="breadcrumbs">
            <li class="">Home</li>
            <li class="">Sambutan</li>
            <li class=""><?php echo $d['judul']; ?></li>
          </ul>
          <h2 class="page-title"><?php echo $d['judul']; ?></h2>
          <div id="post_content" class="post_content" role="main">
            <article class="type-post hentry">
              <div class="post-info">
                <div class="post_date"><?php echo tgl_indo($d['tanggal']); ?></div>
                <div class="post_views">Di baca <?php echo $d['dibaca']; ?> Kali</div>
              </div>
              <div class="pic post_thumb"> <img src="foto_berita/<?php echo $d['gambar']; ?>" alt="" style="width:100%" > </div>
              <div class="post_content">
                <p>
                    <?php echo $d['isi_berita']; ?>
                </p>
              </div>
              
            </article>

            <?php
            
            if ($d['id_berita']=='614'){
            $profil=mysqli_query($koneksi,"SELECT * FROM berita where id_berita='661'");
            $p=mysqli_fetch_array($profil);
            echo "
            <div id='post_author'>
              <h3>Biografi Ketua Yayasan</h3>
              <div class='photo'><a href='page-author.html'><img alt='' src='foto_berita/$d[gambar]'/></a></div>
              <div class='extra_wrap'>
                <h4>Ust Abdul Qohar, S.Pd</h4>
                <div class='description'>$p[isi_berita].</div>
              </div>
            </div>";
            }
            ?>
           <div class="two_columns_news clearboth"> 
                
                <!-- Recent News -->
                <div class="home_category_news_small clearboth">
                  <div class="border-top"></div>
                  <h2 class="block-title">Artikel Islam</h2>
                  <div class="items-wrap">
					<?php				 
					$ijtima1=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='47' ORDER BY id_berita+1 DESC LIMIT 1");
					$no=1;
					while($i1=mysqli_fetch_array($ijtima1)){      
					
					 $isi_berita = strip_tags($i1['isi_berita']); 
					 $isi = substr($isi_berita,0,170); 
					 $isi = substr($isi_berita,0,strrpos($isi," "));
					
					$tgl=tgl_indo($i1['tanggal']);
					echo "
					  <div class='block_home_post first-post'>
						<div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$i1[judul]'  src='foto_berita/$i1[gambar]' style='width:300px;height:176px'> <span class='link-icon'></span> </a> </div>
						<div class='post-content'>
						  <div class='title'><a href='post-standart.html'>$i1[judul]</a></div>
						</div>
						<div class='post-info'>
						  <div class='post_date'>$tgl</div>
						</div>
						<div class='post-body'>$isi...</div>
					  </div>";
					}
					
                    $ijtima=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='52' ORDER BY id_berita+1 ASC LIMIT 2");
					$no=1;
					while($i=mysqli_fetch_array($ijtima)){      
									
					
					
				  $tgl=tgl_indo($i['tanggal']);
                  echo "
				  <div class='block_home_post bd-bot'>
                    <div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$i[judul]'  src='foto_berita/$i[gambar]' style='width:85px;height:63px'> <span class='link-icon'></span> </a> </div>
                    <div class='post-content'>
                      <div class='title'><a href='post-standart.html'>$i[judul].</a></div>
                    </div>
                    <div class='post-info'>
                      <div class='post_date'>$tgl</div>
                    </div>
                  </div>";
				  }
				  ?>
                    
                  </div>
                </div>
                <!-- /Recent News --> 
                
                <!-- Recent News -->
                <div class="home_category_news_small clearboth">
                  <div class="border-top"></div>
                  <h2 class="block-title">Artikel Pendidikan</h2>
                  <div class="items-wrap">
					<?php				 
					$ijtima1=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='46' ORDER BY id_berita+1 DESC LIMIT 1");
					$no=1;
					while($i1=mysqli_fetch_array($ijtima1)){      
					
					 $isi_berita = strip_tags($i1['isi_berita']); 
					 $isi = substr($isi_berita,0,150); 
					 $isi = substr($isi_berita,0,strrpos($isi," "));
					
					$tgl=tgl_indo($i1['tanggal']);
					echo "
					  <div class='block_home_post first-post'>
						<div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$i1[judul]'  src='foto_berita/$i1[gambar]' style='width:300px;height:176px'> <span class='link-icon'></span> </a> </div>
						<div class='post-content'>
						  <div class='title'><a href='post-standart.html'>$i1[judul]</a></div>
						</div>
						<div class='post-info'>
						  <div class='post_date'>$tgl</div>
						</div>
						<div class='post-body'>$isi...</div>
					  </div>";
					}
					
                    $ijtima=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori='46' ORDER BY id_berita+1 ASC LIMIT 2");
					$no=1;
					while($i=mysqli_fetch_array($ijtima)){      
									
					
					
				  $tgl=tgl_indo($i['tanggal']);
                  echo "
				  <div class='block_home_post bd-bot'>
                    <div class='post-image'><a class='img-link img-wrap w_hover' href='post-standart.html'> <img  alt='$i[judul]'  src='foto_berita/$i[gambar]' style='width:85px;height:63px'> <span class='link-icon'></span> </a> </div>
                    <div class='post-content'>
                      <div class='title'><a href='post-standart.html'>$i[judul].</a></div>
                    </div>
                    <div class='post-info'>
                      <div class='post_date'>$tgl</div>
                    </div>
                  </div>";
				  }
				  ?>
					
                  </div>
                </div>
                <!-- /Recent News --> 
              </div>
            
          </div>