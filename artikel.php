                
                  
<section id="blog_posts" class="home_category_news clearboth">
  <div class="border-top"></div>
  <h2 class="block-title">Artikel Islam dan Pendidikan</h2>
  <?php				 
  $artikel=mysqli_query($koneksi,"SELECT * FROM berita WHERE id_kategori in ('46', '47') ORDER BY id_berita DESC LIMIT 3");
  $no=1;
    while($art=mysqli_fetch_array($artikel)){      
      $isi_berita = strip_tags($art['isi_berita']); 
      $isi = substr($isi_berita,0,150); 
      $isi = substr($isi_berita,0,strrpos($isi," "));

      $tgl=tgl_indo($art['tanggal']);

      echo "
      <article>
      <div class='pic'><a href='detail-$art[id_berita]-$art[judul_seo].html' class='w_hover img-link img-wrap'><img src='foto_berita/$art[gambar]' alt='' /> <span class='link-icon'></span> </a> </div>
      <h3><a href='detail-$art[id_berita]-$art[judul_seo].html'>$art[judul]</a></h3>
      <div class='post-info'><a href='' class='post_submitted'>Posted by $art[username]</a><a href='#' class='post_date'>$tgl</a></div>
      <div class='text'> $isi...</div>
      <a href='detail-$art[id_berita]-$art[judul_seo].html' class='more-link'>Selengkapnya<span></span></a> 
      </article>";
    }
  ?>
</section> 